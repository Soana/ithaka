var TILE_WIDTH = 124;
var TILE_HEIGHT = 88;
var LANG = "DE";
 
var TILES_X;
var TILES_Y;
var HEIGHT = 0;
var WIDTH = 0;

var DIST_HEIGHT = 32;
var DIST_WIDTH = 64;

var MAP = null;
var WALL = null;
var TILEPROTOTYPES = null;
var DICT = null;
var PLANS = null;
var M_X;
var M_Y;
var PERM_BANNED 	= 0;
var PERM_USER 		= 1;
var PERM_OPERATOR 	= 2;
var PERM_ADMIN 		= 3;
var PERM_SYSOP 		= 4;

var TILE_PROJECTION_WIDTH = Math.sqrt((TILE_WIDTH * TILE_WIDTH) / 4 + (TILE_HEIGHT * TILE_HEIGHT) / 4);