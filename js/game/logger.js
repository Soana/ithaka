function Logger(wrapper)
{
	wrapper.append("<div id='Logger' class='Logger'></div>");
	this.div = $('#Logger');
}

Logger.prototype.log = function(msg)
{
	this.div.append("<p>" + this.getTime() + msg + "</p>");
	this.div.scrollTop(this.div[0].scrollHeight);
}

Logger.prototype.error = function(msg)
{
	this.div.append("<p style='color: red;'>" + this.getTime() + msg + "</p>");
	this.div.scrollTop(this.div[0].scrollHeight);
}

Logger.prototype.getTime = function()
{
	var d = new Date();
	var h = d.getHours();
	var s = d.getSeconds();
	var m = d.getMinutes();
	var h = d.getHours();
	return "[" + h + ":" + m + ":" + s + "] ";
}