function TileManager(game)
{
	var self = this;
	this.game = game;
	this.tiles = {};
	this.tilesList = Array();
	getWebsocket().addHandler("Tile", function(param)
	{
		var img = new Image();
		img.src = param[2];
		self.register({
			character: param[0],
			name: param[1],
			image: img
		});
	});
};

TileManager.prototype.register = function(tile)
{
	this.game.logger.log("Registered new Tile " + tile.name + " with id "+tile.character);
	if(this.tiles[tile.character] === undefined) this.tilesList.push(tile);
	this.tiles[tile.character] = tile;
};

TileManager.prototype.getTile = function(char)
{
	return this.tiles[char];
};