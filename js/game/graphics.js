var origTileWidth = 124;
var origTileHeight = 88;
var redrawInterval = 20;

function Graphics(width, height, game)
{
	this.canvasWrapper = game.canvasWrapper;
	this.canvasWrapper.append("<canvas width='" + width + "' height='" + height + "' id='canvas'>Your browser does not support HTML5 Canvas</canvas>");
	this.canvas = $('#canvas');
	this.ctx = this.canvas[0].getContext("2d");
	this.game = game;
	this.tileHeight = 88;
	this.highlited = Array();
	this.tileWidth = 124;
	/*this.totalHeight = height * districtHeight * this.tileHeight;
	this.totalWidth = width * districtWidth * this.tileWidth;*/
	this.width = width;
	this.height = height;
	this.cursorPos = {x: 0, y: 0};
	this.zoom = 1;
	this.redraw();
}

Graphics.prototype.zoomOut = function()
{
	if(this.zoom > 0.2)
		this.zoom -= 0.05;
	this.tileHeight = origTileHeight * this.zoom;
	this.tileWidth = origTileWidth * this.zoom;
}

Graphics.prototype.zoomIn = function()
{
	if(this.zoom < 1.2)
		this.zoom += 0.05;
	this.cursorPos.x *= this.zoom;
	this.cursorPos.y *= this.zoom;
	this.tileHeight = origTileHeight * this.zoom;
	this.tileWidth = origTileWidth * this.zoom;
}

Graphics.prototype.redraw = function()
{
	var start = new Date().getTime();
	var self = this;
	this.ctx.clearRect (0, 0, this.width, this.height);
	this.redrawMap();
	this.drawHighlited();
	setTimeout(function()
	{
		self.redraw();
	}, redrawInterval);
	var elapsed = new Date().getTime() - start;
	if(elapsed > redrawInterval)
	{
		this.game.logger.error("Rendering took more than maximum time! " + (elapsed - redrawInterval) + "ms behind!");
	}
}


Graphics.prototype.scroll = function(deltaX, deltaY)
{
	//if(inRange(this.cursorPos.x + deltaX, 0, this.game.map.totalWidth - this.width))
		this.cursorPos.x += deltaX;
	//if(inRange(this.cursorPos.y + deltaY, 0, this.game.map.totalHeight - this.height))
		this.cursorPos.y += deltaY;
}

Graphics.prototype.redrawMap = function()
{
	var map = this.game.map;
	//var offsetTop = (tileHeight / 2) * map.totalHeight;
	for(var y = 0; y < map.totalTilesHeight; y++)
	{
		for(var x = 0; x < map.totalTilesWidth; x++)
		{
			var tile = map.getTileAt(x, y);
			//console.log(tile);
			var drawX = x * this.tileWidth/2 + y * this.tileWidth/2 - this.cursorPos.x;
			var drawY = y * this.tileHeight/2 - x * this.tileHeight/2 - this.cursorPos.y;
			drawY -= this.tileHeight / 2;
			//drawY +=this.totalHeight/2;
			if(	inRange(drawX, - this.tileWidth, this.width + this.tileWidth) && 
				inRange(drawY, - this.tileHeight, this.height + this.tileHeight))
			{
				this.ctx.drawImage(tile.image, drawX, drawY, this.tileWidth, this.tileHeight);
				if(debug) 
				{
					this.ctx.fillStyle = "red";
					this.ctx.font="15px Arial";
					this.ctx.fillText(x+"/"+y, drawX + this.tileWidth/2, drawY + this.tileHeight/2);
				}
			}
			
		}
	}
}
Graphics.prototype.isHighlited = function(x, y)
{
	for(var i = 0; i < this.highlited.length; i++)
	{
		var t = this.highlited[i];
		if(t.x == x && t.y == y) return true;
	}
	return false;
}

Graphics.prototype.drawHighlited = function()
{
	var ctx = this.ctx;
	for(var i = 0; i < this.highlited.length; i++)
	{
		var pos = this.highlited[i];
		var x = pos.x;
		var y = pos.y;
		var drawX = x * this.tileWidth/2 + y * this.tileWidth/2 - this.cursorPos.x;
		var drawY = y * this.tileHeight/2 - x * this.tileHeight/2 - this.cursorPos.y - this.tileHeight/2;
		ctx.strokeStyle = "red";
		ctx.fillStyle = "rgba(255,0,0,0.3)";
		ctx.lineWidth = 1;
		ctx.beginPath();
		ctx.moveTo(drawX + this.tileWidth / 2, drawY);
		ctx.lineTo(drawX + this.tileWidth, drawY + this.tileHeight / 2);
		ctx.lineTo(drawX + this.tileWidth / 2, drawY + this.tileHeight);
		ctx.lineTo(drawX, drawY + this.tileHeight / 2);
		ctx.lineTo(drawX + this.tileWidth / 2, drawY);
		ctx.stroke();
		ctx.fill();
	}
}

Graphics.prototype.getTilePosAt = function(px, py)
{
	var rx = Math.floor((px + this.cursorPos.x)/ this.tileWidth - (py + this.cursorPos.y) / this.tileHeight);
	var ry = Math.floor((py + this.cursorPos.y)/ this.tileHeight + (px + this.cursorPos.x)/ this.tileWidth);
	return {x: rx, y: ry};
}

Graphics.prototype.highlite = function(hx, hy)
{
	this.highlited.push({x: hx, y: hy});
}
Graphics.prototype.clearHighlite = function()
{
	this.highlited = Array();
}

Graphics.prototype.unHighlite = function(x, y)
{
	for(var i = 0; i < this.highlited.length; i++)
	{
		var pos = this.highlited[i];
		if(pos.x == x && pos.y == y)
			this.highlited.splice(i, 1);
	}
}

function inRange(val, lower, upper)
{
	return val >= lower && val <= upper;
}
