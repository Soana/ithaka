function Loader(max, upperDiv)
{
	this.max = max;
	this.wrapper = $("<div class='loader'></div>");
	this.msgBox = $("<div class='msgbox'></div>");
	this.barBox = $("<div class='barbox'></div>");
	this.bar = $("<div class='bar'></div>");
	this.barBox.append(this.bar);
	this.wrapper.append("<h1>Loading</h1>");
	this.wrapper.append(this.barBox);
	this.wrapper.append(this.msgBox);
	this.currentProgress;
	this.updateProgress(0);
	upperDiv.append(this.wrapper);
}

Loader.prototype.updateMsg = function(msg)
{
	this.msgBox.append("<p>"+msg+"</p>");
	this.msgBox.scrollTop(this.msgBox[0].scrollHeight);
}

Loader.prototype.updateProgress = function(val)
{
	var perc = Math.round((val/this.max)*100);
	this.bar.css("width", perc + "%");
	this.currentProgress = val;
}

Loader.prototype.incProgress = function()
{
	this.updateProgress(this.currentProgress+1);
}

Loader.prototype.remove = function()
{
	this.msgBox.remove();
	this.barBox.remove();
	this.bar.remove();
	this.wrapper.remove();
}