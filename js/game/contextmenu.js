function ContextMenu(posX, posY, tiles, game)
{
	this.game = game;
	this.pos = {x: posX, y: posY};
	this.tiles = tiles;
	this.div = $('<div id="context" class="contextmenu"></div>');
	game.canvasWrapper.append(this.div);
}

ContextMenu.prototype.openNormalMenu = function()
{
	var self = this;
	if(self.tiles.length == 1)
	{
		this.addEntry(game.dict.get("action.build"), function() 
		{ 
			self.close();
			self.game.hid.currentMenu = new ContextMenu(self.pos.x, self.pos.y, self.tiles, self.game);
			self.game.hid.currentMenu.openBuildMenu(); 
		});
	}
	this.addEntry(game.dict.get("action.tile"), function() 
	{ 
		self.close();
		self.game.hid.currentMenu = new ContextMenu(self.pos.x, self.pos.y, self.tiles, self.game);
		self.game.hid.currentMenu.openGroundMenu(); 
	});
	this.init();
}

ContextMenu.prototype.openBuildMenu = function()
{
	var self = this;
	this.setLoading();
	getWebsocket().send("GetBlueprints");
	getWebsocket().addHandler("Blueprints", function(response)
	{
		self.unsetLoading();
		for(var i = 0; i < response.length; i++)
		{
			self.addEntry(response[i], function(param) {
				self.game.createConstruction(param, self.tiles[0].x, self.tiles[0].y);
				self.close();
				self.game.hid.currentMenu = undefined;
			}, response[i]);
		}
	});
	this.init();
}

ContextMenu.prototype.setLoading = function()
{
	this.loader = $('<img src="style/ajax-loader.gif" style="margin-left: 11px; margin-top: 5px;" />');
	this.div.append(this.loader);
}

ContextMenu.prototype.unsetLoading = function()
{
	this.loader.remove();
}

ContextMenu.prototype.openGroundMenu = function()
{
	var self = this;
	var tm = this.game.tileMngr;
	for(var i = 0; i < tm.tilesList.length; i++)
	{
		var tile = tm.tilesList[i];
		var helper = $('<div></div>');
		helper.append(tile.image);
		helper.append(tile.name);
		this.addEntry(helper, function(t2) 
		{ 
			for(var i = 0; i < self.tiles.length; i++)
				self.game.map.changeTile(self.tiles[i].x, self.tiles[i].y, t2.character);
			self.close();
			self.game.hid.currentMenu = undefined;
		},tile); 
	}
	this.init();
}

ContextMenu.prototype.init = function()
{
	if(this.pos.x > this.game.canvasWrapper.width()/2) this.pos.x -= this.div.width();
	if(this.pos.y > this.game.canvasWrapper.height()/2) this.pos.y -= this.div.height();
	this.div.css({
		left: this.pos.x,
		top: this.pos.y
	});
}

ContextMenu.prototype.addEntry = function(name, f, param)
{
	var div = $('<div class="entry"></div>');
	div.html(name);
	this.div.append(div);
	div.click(function() {
		f(param);
	})
}

ContextMenu.prototype.close = function()
{
	this.div.remove();
}