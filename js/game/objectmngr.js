function ObjectMngr(game)
{
	var self = this;
	this.game = game;
	getWebsocket().addHandler("Entity", function(param)
	{
		self.addEntity(parseInt(param[0]), parseInt(param[1]), parseInt(param[2]), parseInt(param[3]), param[4], parseInt(param[5]), parseInt(param[6]), parseInt(param[7]), param[8], parseInt(param[9]), parseInt(param[10]));
	});
	this.entities = Array();
}

ObjectMngr.prototype.addEntity = function(dX, dY, x, y, name, sX, sY, rot, img, width, height)
{
	
}

function Entity(dX, dY, pX, pY, name, sX, sY, rot, img, width, height)
{
	this.district = {x: dX, y: dY};
	this.pos = {x: pX, y: pY};
	this.name = name;
	this.spread = {x: sX, y: sY};
	this.rot = rot;
	this.img = img;
	this.siz = {width: width, height: height};
}