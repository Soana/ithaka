var districtWidth = 64;
var districtHeight = 32;

function Map(width, height, name, owner, game)
{
	var self = this;
	this.width = width;
	this.height = height;
	this.name = name;
	this.owner = owner;
	this.map = Array(height);
	this.map[0] = Array(width);
	this.recX = 0;
	this.recY = 0;
	this.game = game; 
	this.totalTilesHeight = height * districtHeight;
	this.totalTilesWidth = width * districtWidth;
	game.logger.log("Map created.");
	getWebsocket().addHandler("TC", function(param)
	{
		self.tileChanged(parseInt(param[0]), parseInt(param[1]), parseInt(param[2]), parseInt(param[3]), param[4]);
	});
}

Map.prototype.tileChanged = function(dx, dy, x, y, char)
{
	this.map[dy][dx].map[y][x] = this.game.tileMngr.getTile(char.charAt(0));
}

Map.prototype.getTileAt = function(x, y)
{
	var distrX = Math.floor(x / districtWidth);
	var distrY = Math.floor(y / districtHeight);
	var innerX = x % districtWidth;
	var innerY = y % districtHeight;
	return this.map[distrY][distrX].map[innerY][innerX];
}

Map.prototype.pushDistrict = function(width, height, name, owner, enc)
{
	if(this.recX == this.width && this.recY == this.height) return;
	this.map[this.recY][this.recX] = new District(width, height, name, owner, enc, this.game);
	this.recX++;
	if(this.recX == this.width)
	{
		this.recY++;
		this.recX = 0;
		if(this.recY == this.height)
		{
			this.game.mapFinished();
		}
		else
			this.map[this.recY] = Array(this.width);
	}
}

Map.prototype.changeTile = function(x, y, char)
{
	var dX = x / districtWidth;
	var dY = y / districtHeight;
	x = x % districtWidth;
	y = y % districtHeight;
	getWebsocket().send("CT", Math.floor(dX), Math.floor(dY), x, y, char);
}

function District(width, height, name, owner, enc, game)
{
	this.game = game;
	this.map = Array(height);
	this.width = width;
	this.height = height;
	this.name = name;
	this.owner = owner;
	this.decode(enc);
}

District.prototype.getTileAt = function(x, y)
{
	return this.map[y][x];
}

District.prototype.decode = function(enc)
{
	for(var y = 0; y < this.height; y++)
	{
		this.map[y] = Array(this.width);
		for(var x = 0; x < this.width; x++)
		{
			this.map[y][x] = game.tileMngr.getTile(enc.charAt(y * this.width + x));
		}
	}
	game.logger.log("Map successfully decoded for District " + this.name);
	game.loader.updateMsg("Map successfully decoded for District " + this.name);
}