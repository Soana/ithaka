var game;

function main()
{
	game = new Game();
}

function Game()
{
	$('.content').remove();
	$('.menu').remove();
	$('body').append("<div class='canvaswrapper' id='canvaswrapper'></div>");
	this.canvasWrapper = $('#canvaswrapper');
	this.inited = false;
	this.dict = new Dictonary();
	this.logger = new Logger(this.canvasWrapper);
	this.map;
	var self = this;
	this.logger.log("Game fired up!");
	this.tileMngr = new TileManager(this);
	
	getWebsocket().addHandler("City", function(param)
	{
		self.init(param[0], param[1], parseInt(param[2]), parseInt(param[3]));
	});
	getWebsocket().addHandler("District", function(param)
	{
		if(this.inited) return;
		self.loader.incProgress();
		var msg = "Decoding district \"" + param[2] + "\"...";
		self.loader.updateMsg(msg);
		self.logger.log(msg);
		self.map.pushDistrict(parseInt(param[0]), parseInt(param[1]), param[2], param[3], param[4]);
	});
	this.objectmngr = new ObjectMngr(this);
}

Game.prototype.createConstruction = function(name, x, y)
{
	var dX = x / districtWidth;
	var dY = y / districtHeight;
	x = x % districtWidth;
	y = y % districtHeight;
	getWebsocket().send("CreateConstruction", name, Math.floor(dX), Math.floor(dY), x, y);
}

Game.prototype.mapFinished = function()
{
	this.loader.remove();
	console.log(this.canvasWrapper.width());
	console.log(this.canvasWrapper.height());
	this.graphics = new Graphics(this.canvasWrapper.width(), this.canvasWrapper.height(), this);
	this.hid = new HID(this.graphics);
}

Game.prototype.init = function(name, owner, width, height)
{
	if(this.inited) return;
	else this.inited = true;
	this.loader = new Loader(width * height, this.canvasWrapper);
	this.logger.log("Received Gameinit");
	this.map = new Map(width, height, name, owner, this);
}