
function HID(graphics)
{
	var self = this;
	this.graphics = graphics;
	this.drag = false;
	this.select = false;
	this.zoom = false;
	this.last = {x: -1, y: -1};
	graphics.canvas.mousedown(function(evt) {self.down(evt.button); return false;});
	graphics.canvas.mouseup(function(evt) { self.up(evt); return false;});
	graphics.canvas.mouseleave(function() { self.up(0);  return false;});
	graphics.canvas.mousemove(function(evt) {self.move(evt); return false;});
	graphics.canvas.mousewheel(function(evt, delta) { if(delta > 0) self.graphics.zoomIn(); else self.graphics.zoomOut();});
	graphics.canvas.bind('contextmenu', function(){ return false; });
	this.currentMenu;
}

HID.prototype.down = function(button)
{
	if(this.currentMenu !== undefined)
	{	
		this.currentMenu.close();
		this.currentMenu = undefined;
	}
	if(button == 2)
	{
		this.drag = true;
		this.zoom = false;
	}
	if(button == 1)
	{
		this.drag = false;
		this.zoom = true;
	}
	if(button == 0)
	{
		this.graphics.clearHighlite();
		this.select = true;
	}
}

HID.prototype.up = function(evt)
{
	if(evt.button == 0)
	{
		this.move(evt);
		this.select = false;
		evt.preventDefault();
		this.currentMenu = new ContextMenu(evt.clientX, evt.clientY, this.graphics.highlited, this.graphics.game);
		this.currentMenu.openNormalMenu();
	}
	else
	{
		this.zoom = false;
		this.drag = false;
		this.last.x = -1;
		this.last.y = -1;
	}
}

HID.prototype.move = function(evt)
{
	if(this.last.x == -1 || this.last.y == -1)
	{
		this.last.x = evt.clientX;
		this.last.y = evt.clientY;
		return;
	}
	if(this.drag)
	{
		this.change(this.last.x - evt.clientX, this.last.y - evt.clientY);
	}
	else if(this.zoom)
	{
		var delta = this.last.x - evt.clientX + this.last.y - evt.clientY;
		if(delta < 0) this.graphics.zoomOut();
		else this.graphics.zoomIn();
	}
	else if(this.select)
	{
		var pos = this.graphics.getTilePosAt(evt.clientX, evt.clientY)
		if(!this.graphics.isHighlited(pos.x, pos.y)) this.graphics.highlite(pos.x, pos.y);
	}
	this.last.x = evt.clientX;
	this.last.y = evt.clientY;
}

HID.prototype.change = function(deltaX, deltaY)
{
	this.graphics.scroll(deltaX, deltaY);
}