function Dictonary()
{
	this.dict = new Array();
	this.load();
};

Dictonary.prototype.load = function()
{
	//		  KEY								GERMAN								ENGLISH
	this.dict["action.tile"] 				 = {"DE" : "Boden ändern",				"US" : "Change ground"};
	this.dict["action.build"] 				 = {"DE" : "Bauen",						"US" : "Build"};
};

Dictonary.prototype.get = function(key)
{
	if(this.dict[key] == null) return "Unknown: "+ key;
	else return this.dict[key][LANG]; 
};