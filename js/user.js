var user;
var rank;

function getUser()
{
	return user;
}

getWebsocket().ready(function()
{
	if(user === undefined)
	{
		var user = getCookie("Username");
		var pw = getCookie("Password");
		if(user != null && pw != null)
			login(user, pw);
	}
});

function isLoggedIn()
{
	user != undefined;
}

function login(username, password)
{
	getWebsocket().send(function(param) {
		if(param[0] == OK)
		{
			if(getCookie("Username") == null || getCookie("Password") == null)
			{
				setCookie("Username", username, 31);
				setCookie("Password", password, 31);
				window.location.reload();
			}
			user = new User(username);
		}
		else
		{
			showMessage(MSG_ERROR, 'Username or password wrong!');
			eraseCookie("Username");
			eraseCookie("Password");
		}
	}, "login", username, password);
}

function User(username)
{
	var self = this;
	this.lang;
	this.money;
	this.mail;
	this.username = username;
	getWebsocket().send(function(param) {
		if(param[0] == OK)
		{
			self.mail = param[1];
			self.lang = param[2];
			self.money = param[3];
			self.refreshDiv();
		}
	}, "retrieve_user");
}

User.prototype.refreshDiv = function()
{
	$('#userInfo').html(this.username+" "+this.lang+" "+this.money);
};