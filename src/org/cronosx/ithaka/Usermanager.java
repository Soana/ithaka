package org.cronosx.ithaka;

import java.io.*;
import java.util.*;

import org.cronosx.ithaka.game.Figur;
import org.cronosx.server.Server;
import org.cronosx.tools.*;

public class Usermanager implements CommandlineListener
{
	private int amount;
	private int amountLoaded;
	private Map<String, User> users;
	private Logger log;
	//private Config conf;
	private File dir;
	private Server server;
	
	/**
	 * Creates a new usermanager and loads all users from the savegamefile specified in the configuration
	 * <p>
	 * @param conf configuration of the server
	 * @param log logger to log to
	 */
	public Usermanager(Config conf, Logger log, Server server)
	{
		users = new HashMap<String, User>();
		this.server = server;
		//this.conf = conf;
		this.log = log;
		dir = new File(conf.getStr("users-dir", "users"));
		if(!dir.exists())
		{
			dir.mkdirs();
			dir.mkdir();
		}
		if(dir.isFile())
		{
			log.error("There is already a file named like the users directory. Unabled to load users!");
		}
		amount = dir.list().length;
	}
	
	public void killAllSockets()
	{
		User[] users = getLoadedUsers();
		for(User u : users)
		{
			UnifiedClient c = u.getClient();
			if(c != null) c.kill();
		}
	}
	
	public User[] getLoadedUsers()
	{
		User[] users = new User[this.users.size()];
		int i = 0;
		for(String s: this.users.keySet())
		{
			users[i++] = this.users.get(s);
		}
		return users;
	}
	
	/**
	 * Returns the user assigned to this name or NULL if no such user exists´
	 * Optionally loads the user if he isn't currently loaded to the memory
	 * <p>
	 * @param name name to look up
	 * @return the user assigned to this name or NULL if no such user exists
	 */
	public User getUser(String name)
	{
		if(users.containsKey(name))
		{
			return users.get(name);
		}
		else 
		{
			return loadUser(name);
		}
	}
	
	/**
	 * Loads an user to the memory and returns it.
	 * If the user doesn't exist, returns null
	 * <p>
	 * @param name name to look up
	 * @return the loaded user or NULL if no such user is existent
	 * 
	 */
	public User loadUser(String name)
	{
		User user = null;
		String filename = server.getSHA1(name);
		File file = new File(dir.getName() + File.separatorChar + filename);
		if(!file.exists()) return null;
		else
		{
			try
			{
				DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
				user = new User(in);
				users.put(name, user);
				amountLoaded++;
				in.close();
			}
			catch(IOException e)
			{
				log.error("Unabled to load userfile \""+filename+"\" for user \""+name+"\" is the file damaged?");
			}
		}
		return user;
	}
	
	/**
	 * Saves the user to disk and unloads it from the memory
	 * <p>
	 * @param user user to unload
	 */
	public void unloadUser(User user)
	{
		unloadUser(user.getName());
	}
	
	/**
	 * Saves the user to disk and unloads it from the memory
	 * <p>
	 * @param name name of the user to unload
	 */
	public void unloadUser(String name)
	{
		saveUser(name);
		users.remove(name);
		amountLoaded--;
	}
	
	/**
	 * Returns whether the specified username is already taken
	 * <p>
	 * @param name name of the user to look up
	 * @return true if the username is already nown to this usermanager
	 */
	public boolean isUserExisting(String name)
	{
		if(users.containsKey(name)) return true;
		else
		{
			String filename = server.getSHA1(name);
			return arrayContains(dir.list(), filename);
		}
	}

	/**
	 * Checks whether the specified user is currently loaded to the memory
	 * <p>
	 * @param name name to look up
	 * @return whether the user is currently loaded
	 */
	public boolean isUserLoaded(String name)
	{
		return users.containsKey(name);
	}
	
	/**
	 * Searches an object in an array and returns true if it is contained
	 * <p>
	 * @param array array to search through
	 * @param obj object to search for
	 * @return true if object is in the array
	 */
	public static boolean arrayContains(Object[] array, Object obj)
	{
		for(Object o: array)
			if(o.equals(obj)) return true;
		return false;
	}
	
	/**
	 * Adds a new user to this usermanager.
	 * The instance of the class user has to be previously created.
	 * If there is already an user registered to this name it will be overwritten
	 * <p>
	 * @param u user to add
	 */
	public void registerNewUser(User u)
	{
		amountLoaded++;
		users.put(u.getName(), u);
	}
	
	/**
	 * Returns the total amount of users known to this server
	 * <p>
	 * @return amount of users
	 */
	public int getUserAmount()
	{
		return amount;
	}
	
	/**
	 * Returns the amount of users that are currently loaded to the memory
	 * <p>
	 * @return amount of users currently loaded
	 */
	public int getUserAmountLoaded()
	{
		return amountLoaded;
	}
	
	/**
	 * Saves all users that are currently loaded to their files
	 */
	public void save()
	{
		log.log("Saving users to user savegamefile...",1);
		int i = 0;
		if(!dir.exists()) dir.mkdir();
		for(String s :users.keySet())
		{
			saveUser(s);
			log.log(++i + "/" + amountLoaded + " users saved", 2);
		}
		log.log("Done.",1);
	}
	
	/**
	 * Saves a user to the harddrive
	 * <p>
	 * @param user user to save
	 */
	public void saveUser(User user)
	{
		String filename = server.getSHA1(user.getName()); 
		File file = new File(dir.getName() + File.separatorChar + filename);
		try
		{
			if(!file.exists()) file.createNewFile();
			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			user.save(out);
			out.close();
		}
		catch(IOException e)
		{
			log.error("Error while saving user \"" + user.getName() + "\" is the file locked?");
		}
	}
	
	/**
	 * Saves a user to the harddrive
	 * <p>
	 * @param name username of the user to save
	 */
	public void saveUser(String name)
	{
		saveUser(getUser(name));
	}
	
	/**
	 * Unloads all possible users from memory
	 */
	public void garbageCollect()
	{
		synchronized(users)
		{
			for(String name:users.keySet())
			{
				User u = users.get(name);
				if(u.canBeUnloaded()) unloadUser(u);
			}
		}
	}

	@Override
	public String commandlineCommand(String command, String... parameters)
	{
		switch(command)
		{
			case "passwd":
			{
				User u = getUser(parameters[0]);
				if(u == null) return "Unknown user \"" + parameters[0] + "\"";
				else 
				{
					u.setPassword(parameters[1]);
					return "Password changed.";
				}
			}
			default: return null;
		}
	}

	@Override
	public void registerCommands(CommandlineInterface commandlineInterface)
	{
		commandlineInterface.registerCommand("passwd", "(user) (password) Changes the password of the specified user"); 
	}
	
	public Figur getFigur(String uniqueName)
	{
		String[] parts = uniqueName.split(".");
		if(parts.length != 2) throw new IllegalArgumentException(uniqueName + " seems not to be a Unique name");
		User user = getUser(parts[0]);
		if(user == null) throw new IllegalArgumentException("Invalid username");
		return user.getFigur(parts[1]);
	}
}
