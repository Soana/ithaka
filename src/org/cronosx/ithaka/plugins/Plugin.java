package org.cronosx.ithaka.plugins;

import java.io.*;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.cronosx.tools.*;

public class Plugin
{
	private File origin;
	private String identifier;
	private Config config;
	private Config info;
	private PluginManager mngr;
	private String[] dependencies;
	private boolean broken;
	private ClassLoader classLoader;
	
	public Plugin(File f, PluginManager mngr)
	{
		this.origin = f;
		this.mngr = mngr;
		broken = false;
		loadConfiguration();
		if(!isBroken()) loadInfo();
		if(!isBroken()) loadDependencies();
	}
	
	public void prepare()
	{
		//if(!isBroken()) compile();
		if(!isBroken()) loadClassFiles();
		if(!isBroken()) initialize();
	}

	private void loadConfiguration()
	{
		File configFile = new File(mngr.getDir().getName()+File.separatorChar + origin.getName()+File.separatorChar+"object.cfg");
		if(configFile.exists())
		{
			config = new Config(configFile);
			identifier = config.getStr("Identifier", null);
			if(identifier != null)
			{
				broken = false;
			}
			else 
			{
				mngr.getServer().getLog().error("Plugin missing Identifier: \"" + origin.getName() + "\"");
				broken = true;
			}
		}
		else 
		{
			mngr.getServer().getLog().error("Plugin missing whole configurationfile \""+origin.getName()+File.separatorChar+"object.cfg"+"\": \"" + origin.getName() + "\"");
			broken = true;
		}
	}
	
	public String getIdentifier()
	{
		return identifier;
	}
	
	private void loadInfo()
	{
		File infoFile = new File(mngr.getDir().getName()+File.separatorChar +origin.getName() + File.separatorChar + "info.cfg");
		if(infoFile.exists())
		{
			info = new Config(infoFile);
		}
		else
			mngr.getServer().getLog().warning("Plugin missing infofile \"info.cfg\": " + origin.getName() + "\"");
		
	}
	
	private void loadDependencies()
	{
		String[] dependencies = config.getStr("Dependencies", "").split(",\\ *");
		this.dependencies = dependencies;
	}
	
	public boolean isBroken()
	{
		return broken;
	}
	
	public String[] getDependencies()
	{
		return dependencies;
	}
	
	private void loadClassFiles(/*File file*/)
	{
		List<URL> urls = new ArrayList<URL>();
		for(File file : origin.listFiles())
		{
			if(file.exists() && file.getName().endsWith(".jar"))
			{
				try
				{
					urls.add(file.toURI().toURL());
				}
				catch(MalformedURLException e)
				{
					broken = true;
					e.printStackTrace();
				}
				mngr.getServer().getLog().log("Plugin \"" + identifier + "\" is loading classfile \"" + file.getName() + "\"", 6);
			}
		}
		URL[] urla =urls.toArray(new URL[]{});
		classLoader= URLClassLoader.newInstance( urla,getClass().getClassLoader());
		mngr.getServer().getLog().log(urls.size() + " classes were loaded by plugin \"" + identifier + "\"", 7);
		
	}
	
	private void initialize()
	{
		String initClass;
		if((initClass = config.getStr("Initializer", null)) != null)
		{
			try
			{
				Class<?> plugin = Class.forName(initClass, true, classLoader);
				Class<? extends Initializer> runClass = plugin.asSubclass(Initializer.class);
				Constructor<? extends Initializer> ctor = runClass.getConstructor();
				Initializer instance = ctor.newInstance();
				instance.setup(mngr.getServer());
			}
			catch (Exception e) 
			{
				broken = true;
				mngr.getServer().getLog().error("Plugin \"" + identifier + "\": Failed to initialize");
				e.printStackTrace();
			}
		}
		else
			mngr.getServer().getLog().log("Plugin \"" + identifier + "\": Did not specify a class for initialization",9);
	}
	
	/*private void compile()
	{
		for(File file : origin.listFiles())
		{
			if(file.getName().endsWith(".java"))
			{
				File classFile = new File(mngr.getDir().getName()+File.separatorChar +origin.getName() + File.separatorChar + file.getName().substring(0, file.getName().length() - 5) + ".class");
				System.out.println(classFile);
				if(!classFile.exists())
				{
					mngr.getServer().getLog().log("Plugin \"" + identifier + "\": Compiling file \"" + file.getName() + "\"", 7);
					try
					{
						compile(file);
					}
					catch(Exception e)
					{
						broken = true;
						mngr.getServer().getLog().error("Plugin \"" + identifier + "\": Failed to compile file \"" + file.getName() + "\"");
					}
				}
			}
		}
	}
	
	private void compile(File file) throws Exception
	{
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		int compilationResult = compiler.run(null, null, null, file.getPath());
		if(compilationResult != 0) throw new Exception();
	}*/
}
