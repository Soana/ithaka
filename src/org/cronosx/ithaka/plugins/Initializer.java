package org.cronosx.ithaka.plugins;

import org.cronosx.ithaka.IthakaServer;

public abstract class Initializer
{
	public abstract void setup(IthakaServer server);
}
