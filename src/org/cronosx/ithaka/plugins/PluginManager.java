package org.cronosx.ithaka.plugins;

import java.io.*;
import java.util.*;

import org.cronosx.ithaka.IthakaServer;

public class PluginManager
{
	private IthakaServer server;
	private Map<String, Plugin> plugins;
	private boolean broken;
	private File dir;
	
	public PluginManager(IthakaServer server)
	{
		broken = false;
		plugins = new HashMap<String, Plugin>();
		this.server = server;
		File pluginsDir = new File(server.getConfig().getStr("plugins-dir", "plugins"));
		dir = pluginsDir;
		if(!pluginsDir.exists()) pluginsDir.mkdirs();
		loadPlugins(pluginsDir);
		checkDependencies();
		compileAndLoad();
	}
	
	public File getDir()
	{
		return dir;
	}
	
	private void compileAndLoad()
	{
		for(String s : plugins.keySet())
		{
			plugins.get(s).prepare();
		}
	}
	
	public boolean isBroken()
	{
		return broken;
	}
	
	private void loadPlugins(File folder)
	{
		getServer().getLog().log("Loading Plugins in folder \""+ folder.getName() + "\"...", 10);
		for(File f : folder.listFiles())
		{
			if(f.isDirectory())
			{
				if(f.getName().toLowerCase().endsWith(".pckg"))
				{
					getServer().getLog().log("Loading Plugin \"" + f.getName() + "\"", 11);
					Plugin p = new Plugin(f, this);
					if(p.isBroken())
					{
						broken = true;
						getServer().getLog().error("Plugin is broken \"" + f.getName() + "\": Skipping plugin");
					}
					else if(hasPlugin(p.getIdentifier()))
					{
						broken = true;
						getServer().getLog().error("Duplicate Identifier \"" + p.getIdentifier() + "\" in plugin \"" + f.getName() + "\": Skipping plugin");
					}
					else
					{
						plugins.put(p.getIdentifier(), p);
						getServer().getLog().log("Successfully loaded plugin \"" + f.getName() + "\"", 11);
					}
				}
				else
				{
					loadPlugins(f);
				}
			}
		}
	}
	
	public boolean hasPlugin(String identifier)
	{
		return plugins.containsKey(identifier);
	}
	
	public IthakaServer getServer()
	{
		return server;
	}
	
	private void checkDependencies()
	{
		for(String name : plugins.keySet())
		{
			Plugin p = plugins.get(name);
			for(String dependency : p.getDependencies())
			{
				if(dependency.length() > 0 && !hasPlugin(dependency))
				{
					broken = true;
					getServer().getLog().error("Unmet dependency \"" + dependency + "\" for plugin \"" + name + "\"");
				}
			}
		}
	}
}
