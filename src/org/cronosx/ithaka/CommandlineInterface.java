package org.cronosx.ithaka;

import java.util.*;

public class CommandlineInterface
{
	
	private CommandlineListener listener;
	private Thread inputListener;
	private Map<String, String> help;
	public CommandlineInterface(CommandlineListener listener)
	{
		help = new HashMap<String, String>();
		this.listener = listener;
		listener.registerCommands(this);
		this.inputListener = new Thread("Inputlistener")
		{
			@Override
			public void run()
			{
				Scanner sc = new Scanner(System.in);
				while(!isInterrupted())
				{
					while(!sc.hasNextLine())
						try
						{
							Thread.sleep(500L);
						}
						catch(InterruptedException e)
						{
							
						}
					parseCommand(sc.nextLine());
				}
				sc.close();
			}
		};
		inputListener.start();
	}
	
	private void parseCommand(String cmd)
	{
		if(cmd.equals("help")) help();
		else 
		{
			String[] parts = cmd.split(" ");
			listener.commandlineCommand(parts[0], Arrays.copyOfRange(parts, 1, parts.length));
		}
	}
	
	private void help()
	{
		System.out.println("The following commands are availabla:");
		System.out.println("help            Prints this message");
		for(String cmd:help.keySet())
		{
			String white = "";
			for(int i = 0; i < 16-cmd.length(); i++) white += " ";
			System.out.println(cmd+white+help.get(cmd));
		}
	}
	
	public void stop()
	{
		inputListener.interrupt();
	}
	
	public void registerCommand(String name, String help)
	{
		this.help.put(name, help);
	}
}
