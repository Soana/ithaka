package org.cronosx.ithaka.game;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import org.cronosx.ithaka.UnifiedClient;
import org.cronosx.ithaka.User;

import org.cronosx.ithaka.content.Building;

public class District
{
	private City city;
	public static final int width = 64; //TODO: Maybe this should be dynamic?
	public static final int height = 32; //TODO: Maybe this should be dynamic?
	private List<Entity> entities;
	private byte[][] ground;
	private String owner;
	private String name;
	private int posX;
	private int posY;
	
	public District(City city, int x, int y)
	{
		entities = new ArrayList<Entity>();
		this.city = city;
		owner = city.getOwnerName();
		this.posX = x;
		this.posY = y;
		name = "District " + x + "/" + y;
		ground = new byte[32][64];
	}
	
	public void tick(long tick)
	{
		for(Entity e: entities)
			e.tick(tick);
	}
	
	public User getOwner()
	{
		return city.getServer().getUsermanager().getUser(owner);
	}
	
	public City getCity()
	{
		return city;
	}
	
	public District(DataInputStream in, City city) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		this.city = city;
		int size = in.readInt();
		entities = new ArrayList<Entity>(size);
		for(int i = 0; i < size; i++)
		{
			Class<?> clazz = Class.forName(in.readUTF(), true, ClassLoader.getSystemClassLoader());
			Class<? extends Entity> runClass = clazz.asSubclass(Entity.class);
			Constructor<? extends Entity> ctor = runClass.getConstructor(DataInputStream.class, District.class);
			Entity e = ctor.newInstance(in, this);
			entities.add(e);
		}
		owner = in.readUTF();
		name = in.readUTF();
		posX = in.readInt();
		posY = in.readInt();
		ground = new byte[height][width];
		for(int i = 0; i < height; i++)
			for(int j = 1; j < width; j++)
				ground[i][j] = in.readByte();
	}

	public int getX()
	{
		return posX;
	}
	
	public int getY()
	{
		return posY;
	}
	
	public void register(Entity e)
	{
		if(!entities.contains(e)) 
		{
			city.broadcast(e.getTransmissionString());
			entities.add(e);
		}
	}
	
	public void unregister(Entity e)
	{
		entities.remove(e); //TODO: Send to client
	}
	
	public void save(DataOutputStream out) throws IOException
	{
		out.writeInt(entities.size());
		for(Entity entity:entities)
		{
			out.writeUTF(entity.getClass().getName());
			entity.save(out);
		}
		out.writeUTF(owner);
		out.writeUTF(name);
		out.writeInt(posX);
		out.writeInt(posY);
		for(int i = 0; i < height; i++)
			for(int j = 1; j < width; j++)
				out.writeByte(ground[i][j]);
	}
	
	public void changeMap(int x, int y, byte code)
	{
		ground[y][x] = code;
		city.broadcast("TC", this.posX, this.posY, x, y, (char)(code + 'A'));
	}

	public int getInfluence(Building building) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void send(UnifiedClient c)
	{
		StringBuilder groundS = new StringBuilder();
		for(int i = 0; i < height; i++)
			for(int j = 0; j < width; j++)
				groundS.append((char) (ground[i][j] + 'A'));
		c.send("District", width, height, name, owner, groundS.toString());
		for(Entity e:entities)
			c.send(e.getTransmissionString());
	}
}
