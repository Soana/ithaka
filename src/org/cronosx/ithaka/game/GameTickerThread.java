package org.cronosx.ithaka.game;

import org.cronosx.ithaka.IthakaServer;

public class GameTickerThread extends Thread
{
	private int TPS;
	private long tick;
	private IthakaServer server;
	
	public GameTickerThread(IthakaServer server)
	{
		this.server = server;
		TPS = server.getConfig().getInt("TPS", 10);
	}
	
	public void run()
	{
		boolean run = true;
		while(!this.isInterrupted() && run)
		{
			long start = System.currentTimeMillis();
			//Timemeasurement start
			City[] citys = server.getCityManager().getLoadedCitys();
			for(City c : citys)
			{
				c.tick(tick);
			}
			tick++;
			//Timemeasuremant stop
			long time = System.currentTimeMillis() - start;
			long timeToSleep = (long)((1/(float)TPS)*1000 - time);
			if(timeToSleep < 0)
			{
				server.getLog().warning("Couldn't keep up. Is the server overloaded or did the system time change? "+ (timeToSleep*-1) + "ms behind");
			}
			else
			{
				try
				{
					Thread.sleep(timeToSleep);
				}
				catch(InterruptedException e)
				{
					interrupt();
					run = false;
				}
			}
		}
	}
}
