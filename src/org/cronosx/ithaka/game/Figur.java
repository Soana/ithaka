package org.cronosx.ithaka.game;

import java.io.*;

import org.cronosx.tools.Config;
import org.cronosx.ithaka.content.*;

public class Figur extends Entity {

	public static enum Action {
		IDLE, BUILDING
	}

	// Skill constants
	public static final int CRAFTING = 0;
	public static final int STRENGTH = 1;
	public static final int INVENTORY = 2;
	public static final int STEALING = 3;
	private static final float[] advance = { -0.1f, 1, 1, 0, 1 };

	private float[] skill = { 1, 5, 1, 0.9f };
	private int[] level = { 0, 0, 0, 0 };

	private int exp = 0;

	private int busyFor = 0;
	private Action busyWith = Action.IDLE;
	private Entity busyAt;
	public Figur(District district) {
		super(district);
	}
	
	public String getUniqueName()
	{
		return getOwnerName()+"."+getName();
	}
	
	public Figur(DataInputStream in, District d) throws IOException
	{
		super(in, d);
		skill = new float[] {in.readFloat(), in.readFloat(), in.readFloat(), in.readFloat()};
		level = new int[] {in.readInt(),in.readInt(),in.readInt(),in.readInt()};
		busyFor = in.readInt();
		busyWith = Action.values()[in.readInt()];
		getOwner().addFigur(this);
	}
	
	public void save(DataOutputStream out) throws IOException
	{
		super.save(out);
		for(float f: skill)
			out.writeFloat(f);
		for(int i: level)
			out.writeInt(i);
		out.writeInt(busyFor);
		out.writeInt(busyWith.ordinal());
	}

	/**
	 * Returns the skill value of a certain skill <br>
	 * Use the skill constants for which
	 * 
	 * @param which
	 *            the skill, whose value to get
	 * @return the value of the skill
	 */
	public float getSkill(int which) {
		return skill[which];
	}

	/**
	 * advances a certain skill to the next level
	 * 
	 * @param skill
	 *            which skill to advance (use the skilling constants)
	 * @return whether the Character had enough points to advance this skill
	 */
	public boolean level(int skill) {
		if (level[skill] * 2 <= exp) {
			switch(busyWith){
				case BUILDING:
					((Building) busyAt).removeWorker(this);
					break;
				default:
					 
			}
			level[skill]++;
			this.skill[skill] += advance[skill];
			exp -= level[skill] * 2;
			switch(busyWith){
				case BUILDING:
					((Building) busyAt).addWorker(this);
					break;
				default:
					 
			}
			return true;
		}
		return false;
	}

	/**
	 * Returns the level of a certain skill <br>
	 * Use the skill constants for which
	 * 
	 * @param which
	 *            the skill, whose level to get
	 * @return the level of the skill
	 */
	public int getLevel(int which) {
		return level[which];
	}

	public int getBusy() {
		return busyFor;
	}

	/**
	 * Makes the Character do something. If with is IDLE, fore is set to 0.
	 * 
	 * @param fore
	 *            how long it is busy
	 * @param with
	 *            with what action is it busy
	 */
	public void setBusy(int fore, Action with, Entity at) {
		if (with != Action.IDLE) {
			this.busyFor = fore;
			busyAt = at;
		} else {
			busyFor = 0;
			busyAt = null;
		}
		this.busyWith = with;
	}

	/**
	 * Resets the time the Character will be busy, unless the action is IDLE, in
	 * which case the time is always 0. <br>
	 * If fore equals 0, the Character is set to IDLE
	 * 
	 * @param fore
	 *            the number of ticks the Character will be busy
	 */
	public void setBusy(int fore) {
		if (busyWith != Action.IDLE) {
			this.busyFor = fore;
			if (fore == 0) {
				busyWith = Action.IDLE;
			}
		}
	}

	@Override
	public void tick(long tick) {
		busyFor--;
		if (busyFor <= 0) {
			busyWith = Action.IDLE;
		}

	}

	@Override
	public String getInitialName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getInitialSpreadX()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getInitialSpreadY()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getInitialImage()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getInitialWidth()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getInitialHeight()
	{
		// TODO Auto-generated method stub
		return 0;
	}

}
