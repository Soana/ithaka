package org.cronosx.ithaka.game;

import java.io.*;
import java.util.*;

import org.cronosx.ithaka.CommandlineInterface;
import org.cronosx.ithaka.CommandlineListener;
import org.cronosx.ithaka.IthakaServer;
import org.cronosx.ithaka.Usermanager;
import org.cronosx.tools.*;

public class CityManager implements CommandlineListener
{
	//private Config conf;
	private Logger log;
	private File dir;
	private int amount;
	private int amountLoaded;
	private Map<String, City> citys;
	private IthakaServer server;
	
	/**
	 * Creates a new citymanager and sets it up
	 * <p>
	 * @param conf config to read from
	 * @param log loagger to log to
	 * @param server main server instance
	 */
	public CityManager(Config conf, Logger log, IthakaServer server)
	{
		this.server = server;
		citys = new HashMap<String, City>();
		//this.conf = conf;
		this.log = log;
		dir = new File(conf.getStr("citys-dir", "citys"));
		if(!dir.exists())
		{
			dir.mkdirs();
			dir.mkdir();
		}
		if(dir.isFile())
		{
			log.error("There is already a file named like the citys directory. Unabled to load citys!");
		}
		amount = dir.list().length;
	}
	
	/**
	 * Loads a city from disk to memory and returns its instance or null if no such city existed
	 * <p>
	 * @param name name of city to load
	 * @return the loaded city or NULL if no such city existed
	 */
	public City loadCity(String name)
	{
		City city = null;
		String filename = server.getSHA1(name);
		File file = new File(dir.getName() + File.separatorChar + filename);
		if(!file.exists()) return null;
		else
		{
			try
			{
				DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
				city = new City(in, server);
				citys.put(name, city);
				amountLoaded++;
				in.close();
			}
			catch(Exception e)
			{
				log.error("Unabled to load cityfile \""+filename+"\" for city \""+name+"\". Is the file damaged or is a plugin missing?");
			}
		}
		return city;
	}
	
	public City[] getLoadedCitys()
	{
		City[] citys = new City[this.citys.size()];
		int i = 0;
		for(String s : this.citys.keySet())
		{
			citys[i++] = this.citys.get(s);
		}
		return citys;
	}
	
	/**
	 * Returns the total amount of citys known to this server
	 * <p>
	 * @return amount of citys
	 */
	public int getAmount()
	{
		return amount;
	}
	
	/**
	 * Looks a city up and returns its instance.
	 * If the city is currently not loaded to memory, loads the city to memory.
	 * If no such city is existing, returns null
	 * <p>
	 * @param name name to look up
	 * @return the city that is associated to this name
	 */
	public City getCity(String name)
	{
		if(citys.containsKey(name)) return citys.get(name);
		else
		{
			return loadCity(name);
		}
	}
	
	/**
	 * Returns whether a city is known to this server
	 * <p>
	 * @param name name to look up
	 * @return true if the city is known to this server
	 */
	public boolean isCityExisting(String name)
	{
		if(citys.containsKey(name)) return true;
		else
		{
			String filename = server.getSHA1(name);
			return Usermanager.arrayContains(dir.list(), filename);
		}
	}
	
	
	/**
	 * @return amount of citys loaded to memory
	 */
	public int getAmountLoaded()
	{
		return amountLoaded;
	}
	
	/**
	 * Unloads a city from memory and saves it
	 * <p>
	 * @param city city to unload
	 */
	public void unloadCity(City city)
	{
		unloadCity(city.getName());
	}
	
	/**
	 * Unloads a city from memory and saves it
	 * <p>
	 * @param city name of city to unload
	 */
	public void unloadCity(String city)
	{
		saveCity(city);
		amountLoaded--;
		citys.remove(city);
	}
	
	/**
	 * Saves a city to harddrive
	 * <p>
	 * @param city name of city to save
	 */
	public void saveCity(String city)
	{
		saveCity(citys.get(city));
	}
	
	/**
	 * Saves a city to harddrive
	 * <p>
	 * @param city city to save
	 */
	public void saveCity(City city)
	{
		String filename = server.getSHA1(city.getName());
		File file = new File(dir.getName() + File.separatorChar + filename);
		try
		{
			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			city.save(out);
			out.close();
		}
		catch(IOException e)
		{
			log.error("Unabled to save cityfile \""+filename+"\" for city \""+city.getName()+"\" is the file locked?");
		}
		
	}
	
	/**
	 * Saves all citys that are currently loaded to harddrive
	 */
	public void save()
	{
		log.log("Saving citys...", 1);
		int i = 0;
		if(!dir.exists()) dir.mkdir();
		for(String s:citys.keySet())
		{
			saveCity(citys.get(s));
			log.log(++i + "/" + amountLoaded + " citys saved", 2);
		}
		log.log("Done.", 1);
	}
	
	/**
	 * Unloads all currently not needed from memory
	 */
	public void garbageCollect()
	{
		for(String name:citys.keySet())
		{
			City city = getCity(name);
			if(city.canBeUnloaded()) citys.remove(city);
		}
	}

	@Override
	public String commandlineCommand(String command, String... parameters)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void registerCommands(CommandlineInterface commandlineInterface)
	{
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * registers a new city to this manager.
	 * The city must have been previously created
	 * <p>
	 * @param city city to register
	 */
	public void createNewCity(City city)
	{
		citys.put(city.getName(), city);
		amount++;
		amountLoaded++;
	}
}
