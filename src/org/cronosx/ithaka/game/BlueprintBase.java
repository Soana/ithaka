package org.cronosx.ithaka.game;

import java.util.*;

import org.cronosx.ithaka.content.Blueprint;

public class BlueprintBase
{
	private Map<String, Blueprint> blueprints;
	public BlueprintBase()
	{
		this.blueprints = new HashMap<String, Blueprint>();
	}
	
	public void registerBlueprint(Blueprint blueprint)
	{
		blueprints.put(blueprint.getName(), blueprint);
	}
	
	public Blueprint getBlueprint(String name)
	{
		return blueprints.get(name);
	}
	
	public Blueprint[] getBlueprints()
	{
		Blueprint[] bps = new Blueprint[blueprints.size()];
		int i = 0;
		for(String s : blueprints.keySet())
		{
			bps[i++] = blueprints.get(s);
		}
		return bps;
	}
}
