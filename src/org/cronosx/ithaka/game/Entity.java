package org.cronosx.ithaka.game;

import java.io.*;
import java.util.ArrayList;

import org.cronosx.ithaka.User;
import org.cronosx.ithaka.Image;
import org.cronosx.tools.Config;
public abstract class Entity {
	//Rotations - the name gives the direction in which the Entity is facing

	public int FRONT = 0;
	public int LEFT = 1;
	public int BACK = 2;
	public int RIGHT = 3;

	private String owner;
	private String name;
	private int spreadX;
	private int spreadY;
	protected District parent;
	private Image defaultImage;

	private int x;
	private int y;
	private int rotation = 0;

	/**
	 * current imagefile
	 */
	private ArrayList<Image> currentImage;

	private long lastTick;

	public Entity(String name, int spreadX, int spreadY, String defaultImage, int imageWidth, int imageHeight, District district) {
		this.name = name;
		this.spreadX = spreadX;
		this.spreadY = spreadY;
		this.parent = district;
		district.register(this);
		this.defaultImage = new Image(defaultImage, imageWidth, imageHeight);
		
		currentImage = new ArrayList<Image>();
		currentImage.add(this.defaultImage);
	}

	public Entity(District district){
		this(district, 0, 0);

	}

	/**
	 * Creates a new Entity form a configuration file <br>
	 * It must contain the keys "name" and "defaultImage" as Strings and the
	 * keys "spreadX", "spreadY", "defaultWidth" and "defaultHeight" as integer
	 * 
	 * @param config
	 *            the Config object of the configuration file
	 */
	public Entity(District district, Integer x, Integer y){
		this.x = x;
		this.y = y;
		name = getInitialName();
		spreadX =  getInitialSpreadX();
		spreadY = getInitialSpreadY();
		this.parent= district;
		district.register(this);
		defaultImage = new Image(getInitialImage(),getInitialWidth(),getInitialHeight());
		currentImage = new ArrayList<Image>();
		currentImage.add(defaultImage);

	}

	public abstract String getInitialName();
	public abstract int getInitialSpreadX();
	public abstract int getInitialSpreadY();
	public abstract String getInitialImage();
	public abstract int getInitialWidth();
	public abstract int getInitialHeight();

	/**
	 * Rotates Entity left or clockwise
	 */
	public void rotateLeft() {
		rotation = ++rotation % 4;
	}

	/**
	 * Rotates Entity right or counter-clockwise
	 */
	public void rotateRight() {
		rotation = (4 + --rotation) % 4;
	}
	
	/**
	 * Returns the position of the Entity within the district
	 * @return an array of x and y
	 */
	public int[] getPosition()
	{
		return new int[]{x,y};
	}
	
	public void save(DataOutputStream out) throws IOException
	{
		out.writeInt(spreadX);
		out.writeInt(spreadY);
		out.writeUTF(name);
		out.writeInt(x);
		out.writeInt(y);
		out.writeInt(rotation);
		out.writeUTF(owner);
		//TODO: Entity must be saved here!
	}

	public Entity(DataInputStream input, District district) throws IOException
	{
		spreadX = input.readInt();
		spreadY = input.readInt();
		name = input.readUTF();
		x = input.readInt();
		y = input.readInt();
		rotation = input.readInt();
		owner = input.readUTF();
		this.parent = district;
		//TODO: Entity must be loaded here
	}
	
	public String getOwnerName()
	{
		return owner;
	}
	
	public User getOwner()
	{
		return parent.getCity().getServer().getUsermanager().getUser(owner);
	}
	
	public String getName() {
		return name;
	}

	public int getRotation() {
		return rotation;
	}

	public void setRotation(int rotation) {
		this.rotation = (rotation % 90) % 4; // oder eine 450 Grad Drehung
		// sollte jemand auf die Idee kommen, da Gradzahlen reinzustecken...
	}

	public int getSpreadX() {
		return spreadX;
	}

	public int getSpreadY() {
		return spreadY;
	}

	public void setPosition(District district, int x, int y) {
		parent.unregister(this);
		this.parent = district;
		district.register(this);
		this.x = x;
		this.y = y;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public District getDistrict() {
		return parent;
	}
	
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Returns the name of the image
	 * 
	 * @return image name
	 */
	public Image getImage(int layer) {
		return currentImage.get(layer);
	}
	
	public Image[] getImages(){
		return currentImage.toArray(new Image[]{});
	}

	public int getImageHeight(int layer) {
		return currentImage.get(layer).getHeight();
	}

	public int getImageWidth(int layer) {
		return currentImage.get(layer).getWidth();
	}

	/**
	 * Clears the current image completely (image AND overlays) and sets the current image to image
	 * @param image the name of the new image
	 */
	public void setImage(String image, int width, int height) {
		currentImage.clear();
		currentImage.add(new Image(image, width, height));
	}
	
	public void setImage(int layer, String image, int width, int height){
		if(layer < 0 || layer >= currentImage.size()){
			currentImage.add(new Image(image, width, height));
		}
		else{
			currentImage.set(layer, new Image(image, width, height));
		}
	}
	
	public void removeImage(int layer){
		if(layer < 0 || layer >= currentImage.size()){
			layer = currentImage.size() - 1;
		}
		currentImage.remove(layer);
	}


	public String getTransmissionString()
	{
		StringBuilder sb = new StringBuilder("Entity ");
		sb.append(parent.getX()).append(" ");
		sb.append(parent.getY()).append(" ");
		sb.append(getX()).append(" ");
		sb.append(getY()).append(" ");
		sb.append("\""+getName()+"\"").append(" ");
		sb.append(getSpreadX()).append(" ");
		sb.append(getSpreadY()).append(" ");
		sb.append(getRotation()).append(" "); //FIXME: Transmissionstring broken since Layers
		sb.append("\""+getInitialImage()+"\"").append(" ");
		sb.append(getInitialWidth()).append(" ");
		sb.append(getInitialHeight()).append(" ");
		return sb.toString();
	}

	/**
	 * action for each tick
	 */
	public void tick(long tick) {
		//TODO: POssible source of error
		if(++lastTick != tick){
			tick(tick);
		}
	}
}
