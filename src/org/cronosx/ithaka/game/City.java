package org.cronosx.ithaka.game;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import org.cronosx.ithaka.IthakaServer;
import org.cronosx.ithaka.UnifiedClient;
import org.cronosx.ithaka.User;

public class City
{
	private String name;
	private String owner;
	private District[][] districts;
	private int width;
	private int height;
	private IthakaServer server;
	private List<String> users;
	
	public City(String owner, String name, int width, int height, IthakaServer server)
	{
		this.server = server;
		users = new ArrayList<String>();
		this.name = name;
		this.owner = owner;
		this.width = width;
		this.height = height;
		districts = new District[height][width];
		for(int x = 0; x < width; x++)
			for(int y = 0; y < height; y++)
				districts[y][x] = new District(this, x, y);
	}
	
	public District getDistrict(int x, int y)
	{
		return districts[y][x];
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public void joinUser(User u)
	{
		users.add(u.getName());
	}
	
	public void leaveUser(User u)
	{
		users.remove(u.getName());
	}
	
	public void broadcast(String s, Object... objs)
	{
		for(String s2: users)
		{
			User u;
			if(s2 != null)
				u = server.getUsermanager().getUser(s2);
			else
			{
				users.remove(s2);
				continue;
			}
			if(u != null && u.getClient() != null) u.getClient().send(s, objs);
		}
	}
	
	public void broadcast(String s)
	{
		for(String s2: users)
		{
			User u;
			if(s2 != null)
				u = server.getUsermanager().getUser(s2);
			else
			{
				users.remove(s2);
				continue;
			}
			if(u != null && u.getClient() != null) u.getClient().send(s);
		}
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public void tick(long tick)
	{
		for(int y = 0; y < height; y++)
			for(int x = 0; x < width; x++)
				districts[y][x].tick(tick);
	}
	
	public IthakaServer getServer()
	{
		return server;
	}
	
	public String getOwnerName()
	{
		return owner;
	}
	
	public City(DataInputStream in, IthakaServer server) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		this.server = server;
		name = in.readUTF();
		owner = in.readUTF();
		width = in.readInt();
		height = in.readInt();
		districts = new District[height][width];
		for(int y = 0; y < height; y++)
			for(int x = 0; x < width; x++)
				districts[y][x] = new District(in, this);
		int numUsers = in.readInt();
		users = new ArrayList<String>(numUsers);
		for(int i = 0; i < numUsers; i++)
			users.add(in.readUTF());
	}
	
	public String getName()
	{
		return name;
	}
	
	public User getOwner()
	{
		return server.getUsermanager().getUser(owner);
	}
	
	public void save(DataOutputStream out) throws IOException
	{
		out.writeUTF(name);
		out.writeUTF(owner);
		out.writeInt(width);
		out.writeInt(height);
		for(int y = 0; y < height; y++)
			for(int x = 0; x < width; x++)
				districts[y][x].save(out);
		out.writeInt(users.size());
		for(String s: users) out.writeUTF(s);
	}
	
	public boolean canBeUnloaded()
	{
		return false; //TODO: May they even be unloaded?
		/*for(String s:users) if(server.getUsermanager().isUserLoaded(s)) return false;
		return true;*/
	}
	
	public void send(UnifiedClient c)
	{
		c.send("City", getName(), getOwnerName(), getWidth(), getHeight());
		for(int i = 0; i < getHeight(); i++)
			for(int j = 0; j < getWidth(); j++)
				districts[i][j].send(c);
	}
}
