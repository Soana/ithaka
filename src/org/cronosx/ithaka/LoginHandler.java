package org.cronosx.ithaka;

public interface LoginHandler
{
	public abstract void login(User u);
}
