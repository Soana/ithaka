package org.cronosx.ithaka;


public class Image {
	private String name;
	private int[] dimension;
	
	public Image(String name, int width, int height){
		this.name = name;
		dimension = new int[]{width, height};
	}
	
	public Image(String name, int[] dimension){
		this.name = name;
		this.dimension = dimension;
	}
	
	public String getName(){
		return name;
	}
	
	public int[] getDimension(){
		return dimension;
	}
	
	public int getWidth(){
		return dimension[0];
	}
	
	public int getHeight(){
		return dimension[1];
	}
}
