package org.cronosx.ithaka;

public interface CommandlineListener
{
	public abstract String commandlineCommand(String command, String... parameters);
	public abstract void registerCommands(CommandlineInterface commandlineInterface);
}
