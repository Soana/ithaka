package org.cronosx.ithaka;

public enum ClientType
{
	Websocket,
	Socket
}
