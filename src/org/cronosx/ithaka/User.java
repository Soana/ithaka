package org.cronosx.ithaka;

import java.io.*;
import java.util.*;

import org.cronosx.ithaka.game.City;
import org.cronosx.ithaka.game.Figur;

public class User
{
	private String name;
	private String password;
	private String mail;
	private Language lang;
	private double money;
	private UnifiedClient client;
	private boolean loggedIn;
	private Map<String, Figur> figurs;
	private City city;
	private Stack<LoginHandler> loginHandlers;
	
	public User(String username, String password, String mail, Language lang)
	{
		figurs = new HashMap<String, Figur>();
		this.name = username;
		this.password = password;
		this.mail = mail;
		this.lang = lang;
		loginHandlers = new Stack<LoginHandler>();
	}
	
	public void setCurrentCity(City city)
	{
		this.city = city;
	}
	
	public City getCurrentCity()
	{
		return city;
	}
	
	/**
	 * Imports a user from a savegamefile
	 * <p>
	 * @param stream stream to read from
	 * @throws IOException
	 */
	public User(DataInputStream stream) throws IOException
	{
		name = stream.readUTF();
		password = stream.readUTF();
		money = stream.readDouble();
		mail = stream.readUTF();
		lang = Language.values()[stream.readInt()];
		loginHandlers = new Stack<LoginHandler>();
	}
	
	public void addLoginHandler(LoginHandler handler)
	{
		loginHandlers.push(handler);
	}
	
	public String getMail()
	{
		return mail;
	}
	
	/**
	 * Exports a user to a savegamefile
	 * <p>
	 * @param stream stream to write to
	 * @throws IOException
	 */
	public void save(DataOutputStream stream) throws IOException
	{
		stream.writeUTF(name);
		stream.writeUTF(password);
		stream.writeDouble(money);
		stream.writeUTF(mail);
		stream.writeInt(lang.ordinal());
	}
	
	public void addFigur(Figur figur)
	{
		figurs.put(figur.getUniqueName(), figur);
	}
	
	public Figur getFigur(String name)
	{
		if(figurs.containsKey(name)) return figurs.get(name);
		else return null;
	}
	
	/**
	 * @return the username
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}
	
	/**
	 * @return the users money
	 */
	public double getMoney()
	{
		return money;
	}
	
	/**
	 * @return true if the user is currently logged in
	 */
	public boolean isLoggedIn()
	{
		return loggedIn;
	}
	
	/**
	 * Checks whether this user can be unloaded.
	 * A user may be unloaded if he is offline and none of his people are currently busy
	 * <p>
	 * @return true if the user can be unloaded
	 */
	public boolean canBeUnloaded()
	{
		return !isLoggedIn();
	}
	
	/**
	 * Changes this users password.
	 * The password has to be previously SHA1 encrypted
	 * <p>
	 * @param passwd password to set (Already SHA1 encrypted)
	 */
	public void setPassword(String passwd)
	{
		this.password = passwd;
	}
	
	/**
	 * Checks whether the supplied password matches this user
	 * <p>
	 * @param password password to test
	 * @return whether the passwords are matching
	 */
	public boolean passwordMatches(String password)
	{
		return password.equals(this.password);
	}
	
	public void login(UnifiedClient client)
	{
		this.client = client;
		this.loggedIn = true;
		client.addHandler("retrieve_user", new Command() {
			public Response execute(String[] param)
			{
				return new Response(Code.OK, mail, lang.ordinal(), money);
			}
		});
		while(!loginHandlers.isEmpty())
			loginHandlers.pop().login(this);
	}
	
	public void logout()
	{
		if(city != null) city.leaveUser(this);
		city = null;
		client = null;
		loggedIn = false;
	}
	
	public UnifiedClient getClient()
	{
		return client;
	}
}
