package org.cronosx.ithaka.frontend;

import org.cronosx.cgi.Content;
import org.cronosx.cgi.Page;
import org.cronosx.cgi.components.*;
public class ContentLogin extends Content
{

	public ContentLogin(Page page)
	{
		super(page);
		add(new ComponentHeadline("Login", 1));
		ComponentFormJavascript form = new ComponentFormJavascript();
		form.addEntry("Username", new ComponentInputText("username"))
			.addValidator(new InputValidatorLength(3,16))
			.addValidator(new InputValidatorConsistsOf("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"));
		form.addEntry("Password", new ComponentInputPassword("password"))
			.addValidator(new InputValidatorLength(8));
		form.addEntry("Login", new ComponentInputButton("ok", "OK", "formLogin();"))
			.addValidator(new InputValidatorAll(form));
		add(form);
		add(new ComponentJavascript(
			"function formLogin()" +
			"{" +
				"login(" +
				"$('input[name=username]').val()," +
				"$('input[name=password]').val());"+
			"}"
		));
	}
	
}
