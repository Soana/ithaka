package org.cronosx.ithaka.frontend;

import org.cronosx.cgi.*;
import org.cronosx.cgi.components.*;

public class ContentRegister extends Content
{

	public ContentRegister(Page page)
	{
		super(page);
		ComponentFormJavascript form = new ComponentFormJavascript();
		form.addEntry("Username", new ComponentInputText("username"))
			.addValidator(new InputValidatorLength(3,16))
			.addValidator(new InputValidatorConsistsOf("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"));
		ComponentInputPassword pw1 = new ComponentInputPassword("pw1"); 
		form.addEntry("Password", pw1)
		.addValidator(new InputValidatorLength(8));
		form.addEntry("Repeat", new ComponentInputPassword("pw2"))
			.addValidator(new InputValidatorEquals(pw1));
		form.addEntry("Mail", new ComponentInputText("mail"))
			.addValidator(new InputValidatorMail());
		ComponentInputDropDown lang = new ComponentInputDropDown("lang");
		lang.addEntry("German");
		lang.addEntry("English");
		form.addEntry("Language", lang);
		form.addEntry("Register", new ComponentInputButton("ok", "OK", "register();"))
			.addValidator(new InputValidatorAll(form));
		add(new ComponentHeadline("Register", 1));
		add(new ComponentJavascript(
				"function register() " +
				"{" +
					"getWebsocket().send(function(param) {" +
						"if(param[0] == OK)" +
						"{" +
							"showMessage(MSG_OK, 'Registration was successfully', 'login');" +
						"}" +
						"if(param[0] == Argument_Error)" +
						"{" +
							"showMessage(MSG_ERROR, 'Username was already taken or anything else went wrong');" +
						"}" +
				"}, 'register', " +
						"$('input[name=username]').val(), " +
						"$('input[name=pw1]').val(), " +
						"$('input[name=pw2]').val(), " +
						"$('input[name=mail]').val(), " +
						"$('select[name=lang]').val());" +
				"};"));
		add(form);
	}
	
}
