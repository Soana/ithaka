package org.cronosx.ithaka.frontend;

import org.cronosx.cgi.Content;
import org.cronosx.cgi.Page;
import org.cronosx.cgi.components.*;
import org.cronosx.ithaka.IthakaServer;
import org.cronosx.ithaka.game.City;

public class ContentLookupCity extends Content
{

	public ContentLookupCity(Page page)
	{
		super(page);
		IthakaServer server = (IthakaServer)page.getWebserver().getServer();
		add(new ComponentHeadline("Currently loaded Citys (" + server.getCityManager().getAmountLoaded() + "/" + server.getCityManager().getAmount() + ")", 1));
		String s = "";
		for(City c : server.getCityManager().getLoadedCitys())
			s += c.getName();
		add(new ComponentText(s));
		add(new ComponentHeadline("Lookup City", 1));
		ComponentForm form = new ComponentForm("lookupCity");
		form.addEntry("Name", new ComponentInputText("name"));
		form.addEntry("Lookup", new ComponentInputSubmit("OK"));
		add(form);
		if(page.getPostData().containsKey("name"))
		{
			String name = page.getPostData().get("name").toString();
			if(server.getCityManager().isCityExisting(name))
			{
				City city = server.getCityManager().getCity(name);
				add(new ComponentText("City is existing" + "\n" +
						"Name: " + city.getName() + "\n" +
						"Owner: " + city.getOwnerName() + "\n"));
				add(new ComponentLink("game?city=" + name, "Join"));
			}
			else
				add(new ComponentText("This city is not existing"));
		}
	}
	
}
