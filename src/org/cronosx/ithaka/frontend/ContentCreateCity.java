package org.cronosx.ithaka.frontend;

import org.cronosx.cgi.Content;
import org.cronosx.cgi.Page;
import org.cronosx.cgi.components.ComponentForm;
import org.cronosx.cgi.components.ComponentHeadline;
import org.cronosx.cgi.components.ComponentInputSubmit;
import org.cronosx.cgi.components.ComponentInputText;
import org.cronosx.cgi.components.ComponentText;
import org.cronosx.ithaka.IthakaServer;
import org.cronosx.ithaka.game.City;

public class ContentCreateCity extends Content
{

	public ContentCreateCity(Page page)
	{
		super(page);
		IthakaServer server = (IthakaServer) page.getWebserver().getServer();
		add(new ComponentHeadline("Create new City", 1));
		ComponentForm form = new ComponentForm("createCity");
		form.addEntry("Name", new ComponentInputText("name"));
		form.addEntry("Owner", new ComponentInputText("owner"));
		form.addEntry("Districts (X)", new ComponentInputText("x"));
		form.addEntry("Districts (Y)", new ComponentInputText("y"));
		form.addEntry("Create", new ComponentInputSubmit("OK"));
		add(form);
		if(page.getPostData().containsKey("name"))
		{
			String name = page.getPostData().get("name").toString();
			String owner = page.getPostData().get("owner").toString();
			try
			{
				if(server.getCityManager().isCityExisting(name))
					add(new ComponentText("This city is already existing"));
				else
					if(!server.getUsermanager().isUserExisting(owner))
						add(new ComponentText("The specified user \"" + owner + "\"does not exist"));
					else
					{
						int x = Integer.parseInt(page.getPostData().get("x").toString());
						int y = Integer.parseInt(page.getPostData().get("y").toString());
						City c = new City(owner, name, x, y, server);
						server.getCityManager().createNewCity(c);
						add(new ComponentText("New city created successfully"));
					}
			}
			catch(NumberFormatException e)
			{
				add(new ComponentText("City couldn't be created: Bad number format"));
			}
		}
	}
	
}
