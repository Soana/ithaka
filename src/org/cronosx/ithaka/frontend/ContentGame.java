package org.cronosx.ithaka.frontend;

import org.cronosx.cgi.Content;
import org.cronosx.cgi.Page;
import org.cronosx.cgi.components.ComponentJavascript;
import org.cronosx.ithaka.IthakaServer;
import org.cronosx.ithaka.LoginHandler;
import org.cronosx.ithaka.User;
import org.cronosx.ithaka.Usermanager;
import org.cronosx.ithaka.game.City;
import org.cronosx.ithaka.game.CityManager;

public class ContentGame extends Content
{

	public ContentGame(Page page)
	{
		super(page);
		String cityName = page.getParam("city");
		String userName = page.getCookie("Username");
		final IthakaServer server = (IthakaServer) page.getWebserver().getServer();
		CityManager cmngr = server.getCityManager();
		Usermanager umngr = server.getUsermanager();
		if(cmngr.isCityExisting(cityName) && umngr.isUserExisting(userName))
		{
			final City city = cmngr.getCity(cityName);
			final User user = server.getUsermanager().getUser(userName);
			user.addLoginHandler(new LoginHandler() {
				@Override
				public void login(User u)
				{
					server.getTileManager().sendTiles(user.getClient());
					city.send(user.getClient());
					city.joinUser(u);
					u.setCurrentCity(city);
				}
				
			});
		}
		add(new ComponentJavascript("main();"));
	}
	
}
