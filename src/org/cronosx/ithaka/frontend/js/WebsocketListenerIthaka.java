package org.cronosx.ithaka.frontend.js;

import org.cronosx.ithaka.IthakaServer;
import org.cronosx.ithaka.UnifiedClient;
import org.cronosx.server.DefaultWebSocketListener;
import org.cronosx.websockets.WebSocket;

public class WebsocketListenerIthaka extends DefaultWebSocketListener
{

	private IthakaServer server;
	public WebsocketListenerIthaka(IthakaServer server)
	{
		super(server);
		this.server = server;
	}

	@Override
	public void onHandshakeSuccessfull(WebSocket origin)
	{
		origin.send("BEER");
	}

	@Override
	protected void parseMessage(String s, WebSocket origin)
	{
		if(s.equals("BEER?"))
		{
			origin.send("BEER!");
			origin.setWebSocketListener(new UnifiedClient(origin, server));
		}
	}
	
}
