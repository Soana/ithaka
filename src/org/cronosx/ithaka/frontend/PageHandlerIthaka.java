package org.cronosx.ithaka.frontend;

import java.io.File;
import java.util.HashMap;

import org.cronosx.cgi.*;
import org.cronosx.cgi.components.ComponentDiv;
import org.cronosx.ithaka.IthakaServer;
import org.cronosx.ithaka.User;
import org.cronosx.webserver.PostData;
import org.cronosx.webserver.Webserver;

public class PageHandlerIthaka implements PageHandler
{

	@Override
	public Page getPage(String request, String pageID, HashMap<String, String> params, String browserName, HashMap<String, String> cookies, String ip, Webserver webserver, CGI cgi, HashMap<String, PostData> postData)
	{
		DefaultPage page = new DefaultPage(request, pageID, params, browserName, cookies, ip, webserver, cgi, postData);
		page.getBody().addCSSFolderToInclude(new File("style"));
		page.getBody().addJSFolderToInclude(new File("js/jquery.js"));
		page.getBody().addJSFolderToInclude(new File("js/jquery.mousewheel.js"));
		page.getBody().addJSFolderToInclude(new File("js/constants.js"));
		page.getBody().addJSFolderToInclude(new File("js/cookies.js"));
		page.getBody().addJSFolderToInclude(new File("js/get.js"));
		page.getBody().addJSFolderToInclude(new File("js/websocket.js"));
		page.getBody().addJSFolderToInclude(new File("js/dictonary.js"));
		page.getBody().addJSFolderToInclude(new File("js/user.js"));
		page.getBody().addJSFolderToInclude(new File("js/popup.js"));
		page.getBody().addJSFolderToInclude(new File("js/validateForms.js"));
		page.getBody().addJSFolderToInclude(new File("js/game"));
		page.getBody().addJSFolderToInclude(new File("config.js"));
		IthakaServer server = (IthakaServer) webserver.getServer();
		if(cookies.containsKey("Username") && cookies.containsKey("Password"))
		{
			User user = server.getUsermanager().getUser(cookies.get("Username"));
			if(user != null && user.passwordMatches(server.getSHA1(cookies.get("Password"))))
			{
				page.addPage("lookupUser", "Lookup User", ContentLookupUsername.class);
				page.addPage("lookupCity", "Lookup City", ContentLookupCity.class);
				page.addPage("createCity", "Create City", ContentCreateCity.class);
				page.addPage("game", ContentGame.class);
			}
			else
			{
				page.setCookie("Username", "");
				page.setCookie("Password", "");
			}
		}
		else
		{
			page.addPage("login", "Login", ContentLogin.class);
			page.addPage("register", "Register", ContentRegister.class);
		}
		page.finalize();
		if(page.getContentDiv() != null) page.getContentDiv().add(new ComponentDiv("websocketStatus",""));
		if(page.getContentDiv() != null) page.getContentDiv().add(new ComponentDiv("userInfo",""));
		return page;
	}
	
}
