package org.cronosx.ithaka.frontend;

import org.cronosx.cgi.*;
import org.cronosx.cgi.components.*;
import org.cronosx.ithaka.*;

public class ContentLookupUsername extends Content
{

	public ContentLookupUsername(Page page)
	{
		super(page);
		IthakaServer server = (IthakaServer)page.getWebserver().getServer();
		add(new ComponentHeadline("Currently loaded users  (" + server.getUsermanager().getUserAmountLoaded() + "/" + server.getUsermanager().getUserAmount() + ")", 1));
		String s = "";
		for(User u : server.getUsermanager().getLoadedUsers())
			s += u.getName() + "\n";
		add(new ComponentText(s));
		add(new ComponentHeadline("Lookup Username", 1));
		add(new ComponentHeadline("Lookup an user", 2));
		ComponentForm form = new ComponentForm("lookupUser");
		form.addEntry("Username (Case-Sensitive)", new ComponentInputText("name",""));
		form.addEntry("Lookup", new ComponentInputSubmit("OK"));
		add(form);
		if(page.getPostData().containsKey("name"))
		{
			String name = new String(page.getPostData().get("name").getContent());
			add(new ComponentHeadline("Lookup for User \"" + name + "\"", 2));
			if(server.getUsermanager().isUserExisting(name))
			{
				add(new ComponentText("User is existing"));
				if(server.getUsermanager().isUserLoaded(name))
					add(new ComponentText("User is loaded"));
				else
					add(new ComponentText("User is NOT loaded"));
				User user = server.getUsermanager().getUser(name);
				add(new ComponentText(	"Hashed Password:" + user.getPassword()+ "\n"+
										"Money:" + user.getMoney() + "\n" + 
										"Mail:" + user.getMail()));
			}
			else
				add(new ComponentText("User is NOT existing"));
		}
	}
	
}
