package org.cronosx.ithaka;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.cronosx.ithaka.content.Blueprint;
import org.cronosx.ithaka.content.Building;
import org.cronosx.ithaka.game.City;
import org.cronosx.ithaka.game.District;
import org.cronosx.websockets.WebSocket;
import org.cronosx.websockets.WebSocketListener;

public class UnifiedClient implements WebSocketListener
{
	private ClientType type;
	private WebSocket originWS;
	private PrintWriter originS;
	private User user;
	private Scanner readerS;
	private IthakaServer server;
	private Map<String, Command> commands;
	
	public UnifiedClient(WebSocket origin, IthakaServer server)
	{
		commands = new HashMap<String, Command>();
		this.server = server;
		this.originWS = origin;
		type = ClientType.Websocket;
		init();
	}
	
	public void kill()
	{
		if(type == ClientType.Websocket)
		{
			if(originWS != null) originWS.close();
		}
	}
	
	public UnifiedClient(final Socket origin, IthakaServer server)
	{
		this.server = server;
		try
		{
			this.readerS = new Scanner(origin.getInputStream());
			this.originS = new PrintWriter(origin.getOutputStream());
			Thread t = new Thread()
			{
				public void run()
				{
					while(origin.isConnected())
					{
						while(!readerS.hasNextLine())
							try
							{
								Thread.yield();
								Thread.sleep(5L);
							}
							catch(InterruptedException e)
							{
								e.printStackTrace();
							}
						onMessage(readerS.nextLine());
					}
				}
			};
			t.start();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		type = ClientType.Socket;
		init();
	}
	
	public void addHandler(String name, Command command)
	{
		commands.put(name, command);
	}
	
	public static String compressRLE(String s)
	{
		StringBuilder fin = new StringBuilder();
		for(int i = 0; i < s.length(); i++)
		{
			int maxWidth = -1;
			int maxAmount = -1;
			String maxString = null;
			for(int width = 1; width < 127 && width < s.length() / 4; width++)
			{
				if(i + width >= s.length()) break;
				String part = s.substring(i, i + width); 
				int amount = 1;
				while(i + (amount + 1) * width < s.length() && 
						s.substring(i + amount * width, i + (amount + 1) * width).equals(part) && 
						amount < 127) 
					amount++;
				if(amount > maxAmount)
				{
					maxAmount = amount;
					maxWidth = width;
					maxString = part;
				}
			}
			if(maxAmount > 3)
			{
				fin.append((char)1).append((char)maxAmount).append((char)maxWidth).append(maxString);
				i += maxAmount*maxWidth-1;
			}
			else fin.append(s.charAt(i));
		}
		return fin.toString();
	}
	
	public static String decompressRLE(String s)
	{
		StringBuilder fin = new StringBuilder();
		for(int i = 0; i < s.length(); i++)
		{
			if(s.charAt(i) == (char)1)
			{
				int amount = (int)s.charAt(++i);
				int width = (int)s.charAt(++i);
				if(i+width >= s.length()) width = s.length() - i - 1;
				String part = s.substring(++i, i+width);
				for(;amount > 0;amount--) fin.append(part);
				i += width -1;
			}
			else fin.append(s.charAt(i));
		}
		return fin.toString();
	}
	
	private void init()
	{
		addHandler("login", new Command() {
			@Override
			public Response execute(String[] param)
			{
				if(param.length == 2)
				{
					return new Response(login(param[0], param[1]));
				}
				return new Response(Code.Invalid_Argument_Count);
			}
			
		});
		addHandler("register", new Command() {
			@Override
			public Response execute(String[] param)
			{
				if(param.length == 5)
				{
					return new Response(register(param[0], param[1], param[2], param[3], param[4]));
				}
				return new Response(Code.Invalid_Argument_Count);
			}
			
		});
		addHandler("close", new Command() {
			@Override
			public Response execute(String[] param)
			{
				if(user != null) user.logout();
				user = null;
				return new Response(Code.OK);
			}
			
		});
		addHandler("ping", new Command() {
			@Override
			public Response execute(String[] param)
			{
				send("pong");
				return new Response(Code.OK);
			}
			
		});
		addHandler("CT", new Command() {
			@Override
			public Response execute(String[] param)
			{
				if(param.length == 5 && user != null)
					return new Response(changeTile(Integer.parseInt(param[0]), Integer.parseInt(param[1]), Integer.parseInt(param[2]), Integer.parseInt(param[3]), param[4].charAt(0)));
				else 
					return new Response(Code.Invalid_Argument_Count);
			}
			
		});
		addHandler("GetBlueprints", new Command() 
		{
			@Override
			public Response execute(String[] param)
			{
				if(param.length == 0)
					return new Response(getBlueprints());
				else 
					return new Response(Code.Invalid_Argument_Count);
			}
			
		});
		addHandler("CreateConstruction", new Command() 
		{
			@Override
			public Response execute(String[] param)
			{
				if(param.length == 5 && user != null)
					return new Response(createConstruction(param[0], Integer.parseInt(param[1]), Integer.parseInt(param[2]), Integer.parseInt(param[3]), Integer.parseInt(param[4])));
				else 
					return new Response(Code.Invalid_Argument_Count);
			}
			
		});
	}
	
	private Code createConstruction(String name, int dX, int dY, int x, int y)
	{
		Blueprint bp = server.getBlueprintBase().getBlueprint(name);
		if(bp == null) return Code.Argument_Error;
		City c = user.getCurrentCity();
		Building b = bp.newInstance(c.getDistrict(dX, dY),x ,y);
		return Code.OK;
	}
	
	private Code getBlueprints()
	{
		Blueprint[] bps = server.getBlueprintBase().getBlueprints();
		String[] args = new String[bps.length];
		for(int i = 0; i < bps.length; i++)
			args[i] = bps[i].getName();
		send("Blueprints", args);
		return Code.OK;
	}
	
	private Code changeTile(int dX, int dY, int x, int y, char chr)
	{
		x = x % District.width;
		y = y % District.height;
		byte code = (byte)(chr- 'A');
		City c = user.getCurrentCity();
		c.getDistrict(dX, dY).changeMap(x, y, code);
		return Code.OK;
	}
	
	public void onMessage(String message)
	{
		message = decompressRLE(message);
		int iOf = message.indexOf(" ");
		String command = message.substring(0, iOf == -1 ? message.length() : iOf);
		String[] param = parseParam(message);
		if(commands.containsKey(command)) send(commands.get(command).execute(param).toString());
		else send("Res " + Code.Unknown_Command.ordinal());
	}
	
	private Code login(String username, String password)
	{
		if(server.getUsermanager().isUserExisting(username))
		{
			User u = server.getUsermanager().getUser(username);
			if(u.passwordMatches(server.getSHA1(password)))
			{
				this.user = u;
				user.login(this);
				return Code.OK;
			}
			else
				return Code.Argument_Error;
		}
		else
			return Code.Argument_Error;
	}
	
	private Code register(String username, String password, String match, String mail, String lang)
	{
		Language l;
		if(username.length() >= 3 && username.length() <= 12 &&
			password.length() >= 4 && password.equals(match) &&
			(l = getLanguage(lang)) != null &&
			!server.getUsermanager().isUserExisting(username))
		{
			User u = new User(username, server.getSHA1(password), mail, l);
			server.getUsermanager().registerNewUser(u);
			return Code.OK;
		}
		else
			return Code.Argument_Error;
	}
	
	private Language getLanguage(String lang)
	{
		for(Language l : Language.values())
		{
			if(l.toString().equals(lang)) return l;
		}
		return null;
	}
	
	private String[] parseParam(String message)
	{
		ArrayList<String> list = new ArrayList<String>();
		int last = message.indexOf(" ");
		int current = 0; 
		while(current != -1)
		{
			current = message.indexOf(" ", last+1);
			if(current != -1) 
			{
				list.add(message.substring(last+1,current));
				if(message.length() > current+1 && message.charAt(current+1) == '"')
				{
					int i = message.indexOf('"', current+2);
					list.add(message.substring(current+2, i));
					current = i+1;
				}
				last = current;
			}
			
		}
		if(last != -1 && last < message.length()) list.add(message.substring(last+1, message.length()));
		return list.toArray(new String[list.size()]);
	}
	
	@Override
	public void onMessage(String s, WebSocket origin)
	{
		onMessage(s);
	}
	
	private void send(String s)
	{
		switch(type)
		{
			case Websocket:
			{
				originWS.send(s);
				break;
			}
			case Socket:
			{
				originS.print(s+"\n");
				originS.flush();
				break;
			}
		}
	}
	
	public void send(String s, Object... objs)
	{
		StringBuilder sb = new StringBuilder(s);
		for(Object o : objs)
		{
			String s2 = o.toString();
			if(s2.contains(" ")) s2 = "\""+s2+"\"";
			sb.append(" ").append(s2);
		}
		send(compressRLE(sb.toString()));
	}

	@Override
	public void onOpen(WebSocket origin) { }

	@Override
	public void onHandshake(WebSocket origin) { }

	@Override
	public void onHandshakeSuccessfull(WebSocket origin) { }

	@Override
	public void onClose(WebSocket origin) { }

}
