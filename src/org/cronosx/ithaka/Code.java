package org.cronosx.ithaka;

public enum Code
{
	OK,
	Invalid_Argument_Count,
	Invalid_Argument_Type,
	Argument_Error,
	Unknown_Command,
	Not_Allowed,
}
