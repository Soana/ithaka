package org.cronosx.ithaka;

public class Response
{
	private Code code;
	private Object[] params;
	public Response(Code code)
	{
		this.code = code;
	}
	
	public Response(Code code, Object... params)
	{
		this.params = params;
		this.code = code;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("Res ");
		sb.append(code.ordinal());
		if(params != null)
		{
			for(Object obj : params)
			{
				String string = obj.toString();
				if(string.indexOf(' ') != -1)
				{
					sb.append(" ").append("\"").append(string).append("\"");
				}
				else
					sb.append(" ").append(string);
			}
		}
		return sb.toString();
	}
}
