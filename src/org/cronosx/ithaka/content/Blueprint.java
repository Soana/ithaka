package org.cronosx.ithaka.content;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.cronosx.ithaka.game.District;

public class Blueprint
{
	private String name;
	private String description;
	private Class<? extends Building> clazz;
	
	public Blueprint(String name, String description, Class<? extends Building> clazz)
	{
		this.name = name;
		this.description = description;
		this.clazz = clazz;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public Building newInstance(District distr, int x, int y)
	{
		try
		{
			Constructor<? extends Building> constr = clazz.getConstructor(District.class, Integer.class, Integer.class);
			return constr.newInstance(distr, x, y);
		}
		catch(InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
