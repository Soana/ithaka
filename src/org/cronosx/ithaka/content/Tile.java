package org.cronosx.ithaka.content;

public abstract class Tile
{
	public abstract String getName();
	private char id;
	public abstract String getImage();
	
	public void setId(char id)
	{
		this.id = id;
	}
	
	public char getId()
	{
		return id;
	}
	
}
