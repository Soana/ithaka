package org.cronosx.ithaka.content;

import org.cronosx.ithaka.game.District;
import org.cronosx.tools.Config;

public abstract class House extends Building
{
	private String collectTaxesImage; 
	private int[] collectDim = new int[2];
	
	public House(District district, Integer x, Integer y) 
	{
		super(district, x, y);
	}

	public float collect(){
		tillMoney = moneyCycle;
		removeImage(-1);
		return currentMoney;
	}
	
	@Override
	public void action() {
		tillMoney --;
		if(tillMoney == 0){
			setImage(-1,collectTaxesImage, collectDim[0], collectDim[1]);
		}
		
	}
	
}
