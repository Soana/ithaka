package org.cronosx.ithaka.content;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;

import org.cronosx.ithaka.game.District;
import org.cronosx.ithaka.game.Entity;
import org.cronosx.ithaka.game.Figur;
import org.cronosx.tools.Config;

public abstract class Building extends Entity 
{
	private final float moneyPerPerson;

	/**
	 * The remaining units to build <br>
	 * LV. 0 Character needs 1 Tick/Unit
	 */
	private int buildingUnits;

	/**
	 * Amount of units the assigned Characters build in one tick
	 */
	private float unitsPerTick = 0;

	/**
	 * Characters currently working on the building
	 */
	private HashMap<String, Figur> workers;

	/**
	 * Array of resources needed for building
	 */
	private int[] resources;
	/**
	 * Temporary storage for surplus building material
	 */
	private int[] tmpResources;
	/**
	 * amount of currently available resources for buildung
	 */
	private int material = 0;
	private double resourcesPerUnit;
	private double resourcesPerTick;
	
	public static final int WOOD = 0;
	public static final int CONCRETE = 1;
	public static final int GLASS = 2;
	public static final int STEEL = 3;


	/**
	 * maximum number of persons to live/work in the Building
	 */
	private final int maxPersons;

	/**
	 * basic costs/taxes per cycle without any influences - positive OR negative
	 */
	private final float basicMoney;

	/**
	 * number of ticks between next taxes can be collected/must be paid
	 */
	protected final int moneyCycle;

	/**
	 * current number of persons living/working in the Building
	 */
	private int currentPersons = 0;

	/**
	 * current amount of costs/taxes for this Building
	 */
	protected float currentMoney;

	/**
	 * current ticks until next collection/payment
	 */
	protected int tillMoney;

	public Building(String name, int x, int y, String image, int imageWidth, int imageHeight,
			int buildingUnits, int[] resources, int maxPersons, int basicMoney, int moneyCycle,
			int moneyPerPerson, District district) {
		super(name, x, y, image, imageWidth, imageHeight, district);
		this.buildingUnits = buildingUnits;
		this.resources = resources;
		this.maxPersons = maxPersons;
		this.basicMoney = basicMoney;
		this.moneyCycle = moneyCycle;
		this.moneyPerPerson = moneyPerPerson;

		currentMoney = basicMoney;
		int res = 0;
		for(int r: resources){
			res += r;
		}
		resourcesPerUnit = (double)res/buildingUnits;
		workers = new HashMap<String, Figur>();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Additionally to the keys which a configuration file for an Entity must
	 * hold, a building needs the keys "units" (units to build; Lv. 0 Character
	 * needs 1 Tick per Unit), one for each possible resource, "maxPersons",
	 * "basicMoney","moneyCycle" and "moneyPerPerson"
	 * 
	 * @param config
	 *            Config object of the configuration file
	 */
	public Building(District district) {
		this(district, 0, 0);
	}
	
	public Building(District district, Integer x, Integer y) {
		super(district, x, y);
		maxPersons = getInitialMaxPersons(); //How many persons max.
		basicMoney = getInitialBasicMoney(); //How many money per taxes
		buildingUnits = getInitialUnits(); //Duration of construction
		moneyCycle = getInitialMoneyWaitTime(); //How many ticks until next taxes
		moneyPerPerson = getInitialMoneyPerPerson(); //How many money per person
		resources = new int[] {}; // TODO fill in resources

		currentMoney = basicMoney;
		int res = 0;
		for(int r: resources){
			res += r;
		}
		resourcesPerUnit = (double)res/buildingUnits;

		workers = new HashMap<String, Figur>();
		// TODO Auto-generated constructor stub
	}

	public abstract int getInitialMaxPersons();
	public abstract float getInitialBasicMoney();
	public abstract int getInitialUnits();
	public abstract int getInitialMoneyWaitTime();
	public abstract float getInitialMoneyPerPerson();
	public abstract float getResearchMoney();
	public abstract int getResearchTime();
	public abstract int getBuildingWood();
	public abstract int getBuildingConcrete();
	public abstract int getBuildingGlass();
	public abstract int getBuildingSteel();

	public Building(DataInputStream in, District district) throws IOException
	{
		//TODO: load resourcesPerUnit
		super(in, district);
		unitsPerTick = in.readFloat();
		currentPersons = in.readInt();
		tillMoney = in.readInt();
		buildingUnits = in.readInt();
		maxPersons = in.readInt();
		basicMoney = in.readFloat();
		moneyCycle = in.readInt();
		moneyPerPerson = in.readFloat();
		resources = new int[] {in.readInt(), in.readInt(), in.readInt(), in.readInt()};
		currentMoney = in.readFloat();
		int workerCount = in.readInt();
		workers = new HashMap<String, Figur>();
		//this.getOwner().getFigure();
		for(int i = 0; i < workerCount; i++)
		{
			workers.put(in.readUTF(), getDistrict().getCity().getServer().getUsermanager().getFigur(in.readUTF())); //TODO: When FigureManager is there, load the figure
		}
	}
	
	@Override
	public void save(DataOutputStream out) throws IOException
	{
		super.save(out);
		out.writeFloat(unitsPerTick);
		out.writeInt(currentPersons);
		out.writeInt(tillMoney);
		out.writeInt(buildingUnits);
		out.writeInt(maxPersons);
		out.writeFloat(basicMoney);
		out.writeInt(moneyCycle);
		out.writeFloat(moneyPerPerson);
		for(int i : resources)
			out.writeInt(i);
		out.writeFloat(currentMoney);
		out.writeInt(workers.size());
		for(String s:workers.keySet())
		{
			out.writeUTF(workers.get(s).getUniqueName());
		}
	}

	public int getCurrentPersons() {
		return currentPersons;
	}

	public void setCurrentPersons(int currentPersons) {
		this.currentPersons = currentPersons;
	}

	public int getUnits() {
		return buildingUnits;
	}

	public int[] getResources() {
		return resources;
	}

	public int getMaxPersons() {
		return maxPersons;
	}

	public float getBasicMoney() {
		return basicMoney;
	}

	public float getCurrentMoney() {
		return currentMoney;
	}

	public void updateCurrentMoney() {
		currentMoney = basicMoney + currentPersons * moneyPerPerson + parent.getInfluence(this);
	}

	/**
	 * when building, makes a Character building at this building
	 * 
	 * @param figur
	 *            Character, who should build
	 */
	public void addWorker(Figur figur) {
		if (!workers.containsKey(figur.getName())) {
			unitsPerTick += 1 / figur.getSkill(Figur.CRAFTING);
			resourcesPerTick = unitsPerTick*resourcesPerUnit;
			workers.put(figur.getName(), figur);
			int time = (int) Math.ceil(buildingUnits / unitsPerTick);
			for (Figur f : workers.entrySet().toArray(new Figur[] {})) {
				f.setBusy(time, Figur.Action.BUILDING, this);
			}
		}
	}

	/**
	 * if the Character is building, stops it
	 * 
	 * @param figur
	 */
	public void removeWorker(Figur figur) {
		Figur x = workers.remove(figur.getName());
		if (x != null) {
			unitsPerTick -= 1 / figur.getSkill(Figur.CRAFTING);
			resourcesPerTick = unitsPerTick*resourcesPerUnit;
			int time = (int) Math.ceil(buildingUnits / unitsPerTick);
			for (Figur f : workers.entrySet().toArray(new Figur[] {})) {
				f.setBusy(time);
			}
		}
	}

	public void bringResources(int resource, int amount){
		if(resources[resource] >= amount){
			material += amount;
			resources[resource] -= amount;
		}
		else{
			material += resources[resource];
			tmpResources[resource] = amount-resources[resource];
			resources[resource] = 0;
		}
	}
	
	public void tick(int tick) {
		super.tick(tick);
		int unitsBefore = buildingUnits;
		if(material >= resourcesPerTick){//ist genug Material vorhanden, um die maximale Anzahl an units zu bauen?
			buildingUnits -= unitsPerTick;
		}
		else{	//wenn nicht,baue so viele Units, wie moeglich und berechne die Bauzeit fuer die worker neu 
			int units = (int)(material/resourcesPerUnit);
			buildingUnits -= units;
			material -= units*resourcesPerUnit;
			int time = (int) Math.ceil(buildingUnits / unitsPerTick);
			for (Figur f : workers.entrySet().toArray(new Figur[] {})) {
				f.setBusy(time);
			}
		}
		
		if (buildingUnits == 0) { // wird nicht mehr gebaut?
			if (buildingUnits != unitsBefore) { // ist der Bau gerade fertig geworden?
				// end building
				for (Figur f : workers.entrySet().toArray(new Figur[] {})) {
					f.setBusy(0);
				}
				workers = null;
				unitsPerTick = 0;
				resourcesPerTick = 0;

				// Starte den normalen Betrieb
				tillMoney = moneyCycle;
				updateCurrentMoney();
			}
			else { // schon laenger fertig
				action();
			}
		}
	}

	/**
	 * action to be taken at every tick, after the building is built
	 */
	public abstract void action();
}
