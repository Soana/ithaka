package org.cronosx.ithaka.content;

import java.util.*;

import org.cronosx.ithaka.IthakaServer;
import org.cronosx.ithaka.UnifiedClient;

public class TileManager
{
	private List<Tile> tiles;
	private IthakaServer server;
	
	public TileManager(IthakaServer server)
	{
		this.server = server;
		tiles = new ArrayList<Tile>();
	}
	
	public void registerTile(Tile t)
	{
		char id = (char) (tiles.size() + 'A');
		tiles.add(t);
		t.setId(id);
		server.getLog().log("Registered new Tile: \"" + t.getName() + "\"", 50);
	}
	
	public void sendTiles(UnifiedClient c)
	{
		for(Tile t : tiles)
		{
			c.send("Tile", t.getId(), t.getName(), server.getPluginManager().getDir().getName() + "/" + t.getImage());
		}
	}
}
