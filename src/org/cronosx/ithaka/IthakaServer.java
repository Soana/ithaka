package org.cronosx.ithaka;

import java.sql.Statement;


import org.cronosx.cgi.*;
import org.cronosx.ithaka.content.TileManager;
import org.cronosx.ithaka.frontend.*;
import org.cronosx.ithaka.frontend.js.*;
import org.cronosx.ithaka.game.*;
import org.cronosx.ithaka.plugins.*;
import org.cronosx.server.*;
import org.cronosx.webserver.*;

public class IthakaServer extends Server implements CommandlineListener
{

	private Usermanager usermanager;
	private CityManager citymanager;
	private Thread saverThread;
	private CommandlineInterface commandlineInterface;
	private GameTickerThread gameTicker;
	private PluginManager plugins;
	private BlueprintBase blueprintBase;
	private TileManager tiles;
	
	/**
	 * Creates a new main server instance
	 * start() must be called before total startup is done.
	 * <p>
	 * @see start()
	 */
	public IthakaServer()
	{
		super();
		blueprintBase = new BlueprintBase();
		citymanager = new CityManager(getConfig(), getLog(), this);
		usermanager = new Usermanager(getConfig(), getLog(), this); 
		commandlineInterface = new CommandlineInterface(this); //Needs citymanager and usermanager to be initialized
		tiles = new TileManager(this);
		gameTicker = new GameTickerThread(this);
		plugins = new PluginManager(this); //Needs everything to be initalized
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run()
			{
				getLog().log("Shutdownhook triggered! Saving all remaining data");
				save();
				shutDown();
			}
		});
		final int saveInterval = getConfig().getInt("save-interval", 1200);
		saverThread = new Thread("Periodic saver") 
		{
			@Override
			public void run()
			{
				try
				{
					Thread.sleep(saveInterval * 1000L);
				}
				catch(InterruptedException e)
				{
					return;
				}
				save();
			}
		};
	
	}
	
	public BlueprintBase getBlueprintBase()
	{
		return blueprintBase;
	}
	
	public TileManager getTileManager()
	{
		return tiles;
	}
	
	/**
	 * @return this servers commandlineInterface
	 */
	public CommandlineInterface getCommandlineInterface()
	{
		return commandlineInterface;
	}
	
	public PluginManager getPluginManager()
	{
		return plugins;
	}
	
	@Override
	public void start()
	{
		super.start();
		saverThread.start();
		gameTicker.start();
	}

	/**
	 * @return this servers usermanager
	 */
	public Usermanager getUsermanager()
	{
		return usermanager;
	}
	
	/**
	 * @return this servers citymanager
	 */
	public CityManager getCityManager()
	{
		return citymanager;
	}
	
	@Override
	protected CGI getDefaultCGIHandler()
	{
		return new CGI(webserver, new PageHandlerIthaka());
	}

	@Override
	public DefaultWebSocketListener getDefaultWebSocketListener()
	{
		return new WebsocketListenerIthaka(this);
	}

	@Override
	public Webserver getDefaultWebserver()
	{
		return new Webserver(getLog(), getConfig(), this);
	}

	@Override
	public void createDatabase(Statement stmt)
	{
		
	}
	
	@Override
	public void save()
	{
		super.save();
		citymanager.save();
		usermanager.save();
		citymanager.garbageCollect();
		usermanager.garbageCollect();
	}
	
	@Override
	public void shutDown()
	{
		commandlineInterface.stop();
		saverThread.interrupt();
		gameTicker.interrupt();
		getUsermanager().killAllSockets();
		super.shutDown();
	}
	
	public static void main(String[] args)
	{
		final IthakaServer server = new IthakaServer();
		server.start();
	}

	@Override
	public String commandlineCommand(String command, String... parameters)
	{
		String result = null;
		switch(command)
		{
			default:
			{
				if((result = usermanager.commandlineCommand(command, parameters)) == null)
					if((result = citymanager.commandlineCommand(command, parameters)) == null)
						return null;
					else return result;
				else return result;
			}
			case "save":
			{
				save();
				return "Everything saved successfully";
			}
			case "shutdown":
			{
				shutDown();
				return "Shutting down...";
			}
		}
	}

	@Override
	public void registerCommands(CommandlineInterface commandlineInterface)
	{
		commandlineInterface.registerCommand("save", "Saves everything to the harddrive");
		commandlineInterface.registerCommand("shutdown", "Shuts down the server");
		usermanager.registerCommands(commandlineInterface);
		citymanager.registerCommands(commandlineInterface);
	}

	@Override
	protected boolean isDatabaseEnabled()
	{
		return false;
	}
}