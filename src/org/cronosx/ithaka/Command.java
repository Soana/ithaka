package org.cronosx.ithaka;

public abstract class Command
{
	public abstract Response execute(String[] param);
}
